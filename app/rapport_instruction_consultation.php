<?php
/**
 * Ce script est utilisé comme variable de remplacement dans les éditions PDF.
 * 
 * Il est inclus/requis par les scripts dyn/varetatpdf.inc et 
 * dyn/varlettretypepdf.inc et permet de populer la variable $consultations
 * avec la liste des consultations d'un dossier pour l'édition du rapport 
 * d'instruction.
 *
 * @package openfoncier
 * @version SVN : $Id: rapport_instruction_consultation.php 5456 2015-11-18 07:55:55Z fmichon $
 */

/**
 * Fichiers requis
 */
// XXX Corriger l'appel de ce script.
require_once "../obj/utils.class.php";
if (!isset($f) && !isset($this->f)) {
    $f = new utils();
} elseif (isset($this->f)) {
    $f = $this->f;
}

// Récupère le numéro de rapport d'instruction
$idx = ($f->get_submitted_get_value('idx') !== null) ? $f->get_submitted_get_value('idx') : "";
$consultations = "<table style='width:100%;'>";

// Si l'identifiant n'est pas vide
if ( !is_null($idx) && $idx != "" && is_numeric($idx) ){
    
    // Récupère les consultations dont un avis a été rendu
    $sqlRapportInstructionConsultattion = "SELECT 
            service.libelle as sl, avis_consultation.libelle as al , consultation.date_retour as cd
        FROM 
            ".DB_PREFIXE."consultation
        LEFT JOIN
            ".DB_PREFIXE."dossier
            ON
            dossier.dossier = consultation.dossier
        LEFT JOIN
            ".DB_PREFIXE."rapport_instruction
            ON 
            dossier.dossier = rapport_instruction.dossier_instruction
        LEFT JOIN
            ".DB_PREFIXE."avis_consultation
            ON
            avis_consultation.avis_consultation = consultation.avis_consultation
        LEFT JOIN
            ".DB_PREFIXE."service
            ON
            service.service = consultation.service
        WHERE
            consultation.avis_consultation IS NOT NULL AND
            rapport_instruction.rapport_instruction = $idx AND
            consultation.visible IS TRUE";
    
    // Exécution de la requête
    $f->addToLog("rapport_instruction_consultation.php : db->query(\"".$sqlRapportInstructionConsultattion."\");", EXTRA_VERBOSE_MODE);
    $resRapportInstructionConsultattion = $f->db->query($sqlRapportInstructionConsultattion);
    $f->isDatabaseError($resRapportInstructionConsultattion);
    
    // Ajout des données récupérées dans la variable de résultat
    while ( $rowRapportInstructionConsultattion=& $resRapportInstructionConsultattion->fetchRow(DB_FETCHMODE_ASSOC) ) {

        $consultations = $consultations . "<tr>".
            "<td style='width:60%;'>" . $rowRapportInstructionConsultattion['sl'] ."</td>" .
            "<td style='width:20%;'>" . $rowRapportInstructionConsultattion['al'] . "</td>" .
            "<td style='width:20%;'>" . $f->formatDate($rowRapportInstructionConsultattion['cd']) . "</td>".
            "</tr>";
    }

    // Récupère les consultations dont aucun avis n'a été rendu
    $sqlRapportInstructionConsultattion = "SELECT 
            service.libelle as sl, avis_consultation.libelle as al , consultation.date_retour as cd
        FROM 
            ".DB_PREFIXE."consultation
        LEFT JOIN
            ".DB_PREFIXE."dossier
            ON
            dossier.dossier = consultation.dossier
        LEFT JOIN
            ".DB_PREFIXE."rapport_instruction
            ON 
            dossier.dossier = rapport_instruction.dossier_instruction
        LEFT JOIN
            ".DB_PREFIXE."avis_consultation
            ON
            avis_consultation.avis_consultation = consultation.avis_consultation
        LEFT JOIN
            ".DB_PREFIXE."service
            ON
            service.service = consultation.service
        WHERE
            consultation.avis_consultation IS NULL AND
            rapport_instruction.rapport_instruction = $idx AND
            consultation.visible IS TRUE";
    
    // Exécution de la requête
    $f->addToLog("rapport_instruction_consultation.php : db->query(\"".$sqlRapportInstructionConsultattion."\");", EXTRA_VERBOSE_MODE);
    $resRapportInstructionConsultattion = $f->db->query($sqlRapportInstructionConsultattion);
    $f->isDatabaseError($resRapportInstructionConsultattion);

    // Ajout des données récupérées dans la variable de résultat
    while ( $rowRapportInstructionConsultattion=& $resRapportInstructionConsultattion->fetchRow(DB_FETCHMODE_ASSOC) ) {

        $consultations = $consultations . "<tr>" .
            "<td style='width:60%;'>" . $rowRapportInstructionConsultattion['sl'] ."</td>" .
            "<td style='width:20%;'>encours</td>" .
            "<td style='width:20%;'>" . $f->formatDate($rowRapportInstructionConsultattion['cd']) . "</td>".
            "</tr>";
    }
}
$consultations .= "</table>";
// Retour des résultats
if ( $consultations != "" ){
        
    $consultations = $consultations;
}
?>