<?php
/**
 * Ce script a pour objet de recuperer l'info passée en paramètre : soit la nature (nouveau dossier,
 * dossier existant), soit le code du type de la demande
 *
 * @package openfoncier
 * @version SVN : $Id: getDemandeInfo.php 4418 2015-02-24 17:30:28Z tbenita $
 */

require_once "../obj/utils.class.php";

$f = new utils("nohtml");
$f->isAccredited(array("demande","demande_modifier","demande_ajouter"), "OR");
$f->disableLog();

// Donnees
$id_demande_type = "";
if ($f->get_submitted_get_value("iddemandetype") != null) {
    $id_demande_type = $f->get_submitted_get_value("iddemandetype");
}
$info = "";
if ($f->get_submitted_get_value("info") != null) {
    $info = $f->get_submitted_get_value("info");
}

// Si les paramètre ne sont pas fournis on stop le traitement
if ($id_demande_type == "" OR $info == "") {
    die();
}
if ($info == "nature") {
    $sql = "SELECT demande_nature.code FROM ".DB_PREFIXE."demande_nature
        INNER JOIN ".DB_PREFIXE."demande_type
            ON demande_type.demande_nature=demande_nature.demande_nature
        WHERE demande_type.demande_type=".intval($id_demande_type);
    $nature = $f->db->getOne($sql);
    $f->addToLog(
        "app/getDemandeInfo.php : db->getOne(\"".$sql."\")",
        VERBOSE_MODE
    );
    $f->isDatabaseError($nature);
    echo $nature;
}
if ($info == "contraintes") {
    $sql = "SELECT demande_type.contraintes FROM ".DB_PREFIXE."demande_type
        WHERE demande_type.demande_type=".intval($id_demande_type);
    $type = $f->db->getOne($sql);
    $f->addToLog(
        "app/getDemandeInfo.php : db->getOne(\"".$sql."\")",
        VERBOSE_MODE
    );
    $f->isDatabaseError($type);
    echo $type;
}
if ($info == "type_aff_form") {
    $sql = "SELECT dossier_autorisation_type.affichage_form
            FROM ".DB_PREFIXE."demande_type
            INNER JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                ON demande_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
            INNER JOIN ".DB_PREFIXE."dossier_autorisation_type
                ON dossier_autorisation_type.dossier_autorisation_type=dossier_autorisation_type_detaille.dossier_autorisation_type
        WHERE demande_type.demande_type=".intval($id_demande_type);
    $type_aff_form = $f->db->getOne($sql);
    $f->addToLog("app/getDemandeInfo.php : db->getOne(\"".$sql."\")", VERBOSE_MODE);
    $f->isDatabaseError($type_aff_form);
    echo $type_aff_form;
}
