<?php
/**
 * Ce script a pour objet de recuperer la liste des pétionnaires correspondant aux critères de recherche
 *
 * @package openfoncier
 * @version SVN : $Id: findArchitecte.php 4418 2015-02-24 17:30:28Z tbenita $
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
$f->isAccredited(array("donnees_techniques","donnees_techniques_modifier","donnees_techniques_ajouter"), "OR");
//Récupération des valeurs envoyées
$f->set_submitted_value();
// Donnees
$nom = ($f->get_submitted_post_value("nom") != null) ? $f->get_submitted_post_value("nom") : "";
$nom = str_replace('*', '', $nom);
$nom = html_entity_decode($nom, ENT_QUOTES);
$nom = $f->db->escapeSimple($nom);

$prenom = ($f->get_submitted_post_value("prenom") != null) ? $f->get_submitted_post_value("prenom") : "";
$prenom = str_replace('*', '', $prenom);
$prenom = html_entity_decode($prenom, ENT_QUOTES);
$prenom = $f->db->escapeSimple($prenom);

$listData = "";

$f->disableLog();

$requete = "frequent is TRUE AND";
if($nom != "") {
    $requete .= " nom ILIKE '%$nom%'";
    $requete .= " AND";
}
if($prenom != "") {
    $requete .= " prenom ILIKE '%$prenom%'";
    $requete .= " AND";
}

$requete = substr($requete, 0, (strlen($requete)-4));

$sql = 
    "SELECT 
        architecte as value,".
        "trim(concat(nom,' ', prenom, ' ', ".
        " cp, ' ', ville)) as content ".
    "FROM ".DB_PREFIXE."architecte WHERE ".$requete;

$res = $f->db->query($sql);
$f->isDatabaseError($res);
$listData=array();
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
    $listData[] = $row;
}

echo json_encode($listData);

?>