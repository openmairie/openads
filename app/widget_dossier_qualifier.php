<?php
/**
 * Widget - 
 *
 * @package openfoncier
 * @version SVN : $Id: widget_dossier_qualifier.php 4570 2015-04-10 16:12:43Z nmeucci $
 */

require_once "../obj/utils.class.php";
if (!isset($f)) {
    $f = new utils(NULL, "dossier_qualifier", 
    _("Widget - Mes Dossiers A Qualifier"));
}

$nbJours = 15;

// Création de la requête de récupération du nombre de dossiers à qualifier ERP
$sql = 
    "SELECT 
        count(dossier)
    FROM
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."dossier_instruction_type
            ON dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
        ";

    // Si l'utilisateur n'est pas un qualificateur
    // alors on n'affiche que ses dossiers à qualifier
    if(!$f->isUserQualificateur()) {
        $sql .= " JOIN
            ".DB_PREFIXE."instructeur
            ON
                dossier.instructeur = instructeur.instructeur 
        JOIN
            ".DB_PREFIXE."om_utilisateur
            ON
                instructeur.om_utilisateur = om_utilisateur.om_utilisateur ";
    }

    $sql .= " WHERE ";

    // Si l'utilisateur n'est pas un qualificateur
    // alors on n'affiche que ses dossiers à qualifier
    if(!$f->isUserQualificateur()) {
        $sql .= " om_utilisateur.login = '".$f->clean_break($_SESSION['login'])."' AND ";
    }

    // Sinon si collectivité de l'utilisateur niveau mono alors filtre sur celle-ci
    elseif ($f->isCollectiviteMono($_SESSION['collectivite']) === true) {
        $sql .= " dossier.om_collectivite=".$_SESSION['collectivite']." AND ";
    }

    $sql .= " dossier.a_qualifier IS TRUE AND
        dossier.erp IS TRUE AND
        dossier_instruction_type.sous_dossier IS NOT TRUE";

// Exécution de la requête
$erp = $f->db->getOne($sql);
$f->addToLog("app/widget_dossiers_qualifier.php: db->getOne(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($erp);

// Création de la requête de récupération du nombre de dossiers à qualifier ADS
$sql = 
    "SELECT 
        count(dossier)
    FROM
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."dossier_instruction_type
            ON dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
    ";

    // Si l'utilisateur n'est pas un qualificateur
    // alors on n'affiche que ses dossiers à qualifier
    if(!$f->isUserQualificateur()) {
        $sql .= " JOIN
            ".DB_PREFIXE."instructeur
            ON
                dossier.instructeur = instructeur.instructeur 
        JOIN
            ".DB_PREFIXE."om_utilisateur
            ON
                instructeur.om_utilisateur = om_utilisateur.om_utilisateur ";
    }

    $sql .= " WHERE ";

    // Si l'utilisateur n'est pas un qualificateur
    // alors on n'affiche que ses dossiers à qualifier
    if(!$f->isUserQualificateur()) {
        $sql .= " om_utilisateur.login = '".$f->clean_break($_SESSION['login'])."' AND ";
    }

    // Sinon si collectivité de l'utilisateur niveau mono alors filtre sur celle-ci
    elseif ($f->isCollectiviteMono($_SESSION['collectivite']) === true) {
        $sql .= " dossier.om_collectivite=".$_SESSION['collectivite']." AND ";
    }

    $sql .= " dossier.a_qualifier IS TRUE AND
        dossier.erp IS FALSE AND
        dossier_instruction_type.sous_dossier IS NOT TRUE";

// Exécution de la requête
$ads = $f->db->getOne($sql);
$f->addToLog("app/widget_dossiers_qualifier.php: db->getOne(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($ads);

// Affiche des données résultats
if ( $erp + $ads > 0 ){
    
    //Nombre de dossiers à qualifier
    $message = _("Vous avez ").( $erp + $ads )._(" dossier(s) a qualifier :<br/>");
    $message .= (isset($erp) && $erp > 0 ) ? " - ".$erp._(" ERP")."<br/>" : "" ;
    $message .= ((isset($ads) && $ads > 0 ) ? " - ".$ads._(" ADS")."<br/>" : "")."<br/>" ;
    echo $message;

    $footer = OM_ROUTE_TAB."&obj=dossier_qualifier";

    // Si l'utilisateur est un qualificateur
    // alors on affiche tous les dossiers à qualifier
    if($f->isUserQualificateur()) {
        $footer = OM_ROUTE_TAB."&obj=dossier_qualifier_qualificateur";
    }
    $footer_title = _("Voir tous mes dossiers a qualifier");
}
else{
    
    echo _("Vous n'avez pas de dossiers a qualifier.");
}
?>