<?php
/**
 * Ce script a pour objet de recuperer la liste des pétionnaires correspondant aux critères de recherche
 *
 * @package openfoncier
 * @version SVN : $Id: findPetitionnaire.php 5710 2016-01-05 17:41:54Z jymadier $
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
$f->isAccredited(array("demande","demande_modifier","demande_ajouter"), "OR");
//Récupération des valeurs envoyées
$f->set_submitted_value();
$f->disableLog();

// Donnees
$par_nom = ($f->get_submitted_post_value("particulier_nom") != null) ? $f->get_submitted_post_value("particulier_nom") : "";
$par_nom = str_replace('*', '', $par_nom);
$par_nom = html_entity_decode($par_nom, ENT_QUOTES);
$par_nom = $f->db->escapeSimple($par_nom);

$par_prenom = ($f->get_submitted_post_value("particulier_prenom") != null) ? $f->get_submitted_post_value("particulier_prenom") : "";
$par_prenom = str_replace('*', '', $par_prenom);
$par_prenom = html_entity_decode($par_prenom, ENT_QUOTES);
$par_prenom = $f->db->escapeSimple($par_prenom);

$mor_raison_sociale = ($f->get_submitted_post_value("personne_morale_raison_sociale") != null) ? $f->get_submitted_post_value("personne_morale_raison_sociale") : "";
$mor_raison_sociale = str_replace('*', '', $mor_raison_sociale);
$mor_raison_sociale = html_entity_decode($mor_raison_sociale, ENT_QUOTES);
$mor_raison_sociale = $f->db->escapeSimple($mor_raison_sociale);

$mor_denomination = ($f->get_submitted_post_value("personne_morale_denomination") != null) ? $f->get_submitted_post_value("personne_morale_denomination") : "";
$mor_denomination = str_replace('*', '', $mor_denomination);
$mor_denomination = html_entity_decode($mor_denomination, ENT_QUOTES);
$mor_denomination = $f->db->escapeSimple($mor_denomination);

$mor_siret = ($f->get_submitted_post_value("personne_morale_siret") != null) ? $f->get_submitted_post_value("personne_morale_siret") : "";
$mor_siret = str_replace('*', '', $mor_siret);
$mor_siret = html_entity_decode($mor_siret, ENT_QUOTES);
$mor_siret = $f->db->escapeSimple($mor_siret);

$mor_cat_juridique = ($f->get_submitted_post_value("personne_morale_categorie_juridique") != null) ? $f->get_submitted_post_value("personne_morale_categorie_juridique") : "";
$mor_cat_juridique = str_replace('*', '', $mor_cat_juridique);
$mor_cat_juridique = html_entity_decode($mor_cat_juridique, ENT_QUOTES);
$mor_cat_juridique = $f->db->escapeSimple($mor_cat_juridique);

$mor_nom = ($f->get_submitted_post_value("personne_morale_nom") != null) ? $f->get_submitted_post_value("personne_morale_nom") : "";
$mor_nom = str_replace('*', '', $mor_nom);
$mor_nom = html_entity_decode($mor_nom, ENT_QUOTES);
$mor_nom = $f->db->escapeSimple($mor_nom);

$mor_prenom = ($f->get_submitted_post_value("personne_morale_prenom") != null) ? $f->get_submitted_post_value("personne_morale_prenom") : "";
$mor_prenom = str_replace('*', '', $mor_prenom);
$mor_prenom = html_entity_decode($mor_prenom, ENT_QUOTES);
$mor_prenom = $f->db->escapeSimple($mor_prenom);

$om_collectivite = ($f->get_submitted_post_value("om_collectivite") != null) ? $f->get_submitted_post_value("om_collectivite") : $_SESSION['collectivite'];
$listData = "";

$requete = "frequent is TRUE AND 
            type_demandeur = 'petitionnaire' AND";
if($par_nom != "") {
    $requete .= " particulier_nom ILIKE '%$par_nom%'";
    $requete .= " AND";
}
if($par_prenom != "") {
    $requete .= " particulier_prenom ILIKE '%$par_prenom%'";
    $requete .= " AND";
}

if ($mor_raison_sociale != "") {
    $requete .= " personne_morale_raison_sociale ILIKE '%$mor_raison_sociale%'";
    $requete .= " AND";
}

if($mor_denomination != "") {
    $requete .= " personne_morale_denomination ILIKE '%$mor_denomination%'";
    $requete .= " AND";
}

if ($mor_siret != "") {
    $requete .= " personne_morale_siret ILIKE '%$mor_siret%'";
    $requete .= " AND";
}

if ($mor_cat_juridique != "") {
    $requete .= " personne_morale_categorie_juridique ILIKE '%$mor_cat_juridique%'";
    $requete .= " AND";
}

if($mor_nom != "") {
    $requete .= " personne_morale_nom ILIKE '%$mor_nom%'";
    $requete .= " AND";
}

if($mor_prenom != "") {
    $requete .= " personne_morale_prenom ILIKE '%$mor_prenom%'";
    $requete .= " AND";
}

// Ajoute une condition sur la collectivité de l'utilisateur, ou celle de niveau 2
$requete .= ' (om_collectivite = '.$om_collectivite;
$requete .= ' OR om_collectivite = (
    SELECT om_collectivite from '.DB_PREFIXE.'om_collectivite
    WHERE niveau=\'2\'
))';

$sql = 'SELECT
    demandeur as value,
    trim(concat(particulier_nom,\' \', particulier_prenom, \' \',
    personne_morale_raison_sociale, \' \', personne_morale_denomination,
    \' \', personne_morale_categorie_juridique, \' \', personne_morale_siret, \' \',
    personne_morale_nom, \' \', personne_morale_prenom, \' \',
    code_postal, \' \', localite)) as content
FROM '.DB_PREFIXE.'demandeur
WHERE '.$requete;

$res = $f->db->query($sql);
$f->isDatabaseError($res);
$listData=array();
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
    $listData[] = $row;
}

echo json_encode($listData);

?>