--
-- PostgreSQL database dump
--

-- Dumped from database version 10.22 (Ubuntu 10.22-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.22 (Ubuntu 10.22-0ubuntu0.18.04.1)

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET xmloption = content;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- Data for Name: demande_nature; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO demande_nature (demande_nature, code, libelle, description) VALUES (1, 'NOUV', 'Nouveau dossier', 'Une demande de nature ''Nouveau dossier'' doit donner lieu à la création d''un dossier d''autorisation');
INSERT INTO demande_nature (demande_nature, code, libelle, description) VALUES (2, 'EXIST', 'Dossier existant', 'Une demande de nature ''Dossier existant'' doit être reliée à un dossier d''autorisation existant');


--
-- Data for Name: demande_type; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (1, 'DI', 'Dépôt Initial', '', 1, 1, 1, 1, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (2, 'DI', 'Dépôt Initial', '', 1, 1, 6, 2, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (3, 'DI', 'Dépôt Initial', '', 1, 1, 11, 3, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (4, 'DI', 'Dépôt Initial', '', 1, 1, 16, 4, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (5, 'DI', 'Dépôt Initial', '', 1, 1, 21, 5, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (6, 'DI', 'Dépôt Initial', '', 1, 1, 26, 6, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (7, 'DI', 'Dépôt Initial', '', 1, 1, 31, 7, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (9, 'DT', 'Demande de transfert', '', 2, 1, 3, 1, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (10, 'DT', 'Demande de transfert', '', 2, 1, 8, 2, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (11, 'DT', 'Demande de transfert', '', 2, 1, 13, 3, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (12, 'DT', 'Demande de transfert', '', 2, 1, 18, 4, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (13, 'DT', 'Demande de transfert', '', 2, 1, 23, 5, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (14, 'DT', 'Demande de transfert', '', 2, 1, 28, 6, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (15, 'DT', 'Demande de transfert', '', 2, 1, 33, 7, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (16, 'DT', 'Demande de transfert', '', 2, 1, 38, 8, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (17, 'DM', 'Demande de modification', '', 2, 1, 2, 1, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (18, 'DM', 'Demande de modification', '', 2, 1, 7, 2, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (19, 'DM', 'Demande de modification', '', 2, 1, 12, 3, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (20, 'DM', 'Demande de modification', '', 2, 1, 17, 4, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (21, 'DM', 'Demande de modification', '', 2, 1, 22, 5, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (22, 'DM', 'Demande de modification', '', 2, 1, 27, 6, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (23, 'DM', 'Demande de modification', '', 2, 1, 32, 7, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (24, 'DM', 'Demande de modification', '', 2, 1, 37, 8, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (8, 'DI', 'Dépôt Initial', '', 1, 1, 36, 8, '', true, 95, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (42, 'DI', 'Dépôt Initial', '', 1, 1, 21, 9, '', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (43, 'DT', 'Demande de transfert', '', 2, 1, 23, 9, 'sans_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (44, 'DM', 'Demande de modification', '', 2, 1, 22, 9, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (25, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 4, 1, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (26, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 9, 2, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (27, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 14, 3, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (28, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 19, 4, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (29, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 24, 5, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (30, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 29, 6, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (31, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 34, 7, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (33, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 5, 1, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (34, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 10, 2, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (35, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 15, 3, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (36, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 20, 4, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (37, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 25, 5, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (38, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 30, 6, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (39, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 35, 7, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (40, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 40, 8, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (32, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 39, 8, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (45, 'DAACT', 'Déclaration attestant l''achèvement et la conformité des travaux', '', 2, 1, 24, 9, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (46, 'DOC', 'Demande d''ouverture de chantier', '', 2, 1, 25, 9, 'avec_recup', true, 1, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (41, 'DPC', 'Dépôt de pièces complémentaire', '', 2, 5, NULL, 8, 'avec_recup', false, 91, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (47, 'DPC', 'Dépôt de pièces complémentaire', '', 2, 1, NULL, 1, 'avec_recup', false, 91, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (48, 'DEP', 'Dépôt Initial REG', '', 1, 2, 46, 10, NULL, false, 104, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (49, 'DEP', 'Dépôt Initial REC', '', 1, 2, 47, 11, NULL, false, 104, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (50, 'DEP', 'Dépôt Initial IN', '', 1, 2, 48, 12, NULL, false, 104, NULL, false);
INSERT INTO demande_type (demande_type, code, libelle, description, demande_nature, groupe, dossier_instruction_type, dossier_autorisation_type_detaille, contraintes, qualification, evenement, document_obligatoire, regeneration_cle_citoyen) VALUES (52, 'DI', 'Dépôt Initial', '', 1, 1, 49, 13, NULL, false, 10, '', false);


--
-- Data for Name: lien_demande_type_etat; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (1, 18, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (2, 17, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (3, 19, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (4, 20, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (5, 21, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (6, 22, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (7, 23, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (8, 24, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (9, 44, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (10, 25, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (11, 26, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (12, 27, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (13, 28, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (14, 29, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (15, 30, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (16, 31, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (17, 33, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (18, 34, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (19, 35, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (20, 36, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (21, 37, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (22, 38, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (23, 39, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (24, 40, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (25, 32, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (26, 45, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (27, 46, 'accepter');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (28, 41, 'incomplet');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (29, 47, 'incomplet');
INSERT INTO lien_demande_type_etat (lien_demande_type_etat, demande_type, etat) VALUES (30, 9, 'accepter');


--
-- Name: demande_nature_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('demande_nature_seq', 3, false);


--
-- Name: demande_type_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('demande_type_seq', 53, false);


--
-- Name: lien_demande_type_etat_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('lien_demande_type_etat_seq', 31, false);


--
-- PostgreSQL database dump complete
--

