--
-- PostgreSQL database dump
--

-- Dumped from database version 10.22 (Ubuntu 10.22-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.22 (Ubuntu 10.22-0ubuntu0.18.04.1)

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET xmloption = content;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- Data for Name: document_numerise_nature; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO document_numerise_nature (document_numerise_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (1, 'INIT', 'Initiale', 'Pièce initiale', NULL, NULL);
INSERT INTO document_numerise_nature (document_numerise_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (2, 'COMP', 'Complémentaire', 'Pièce complémentaire', NULL, NULL);


--
-- Data for Name: document_numerise_type_categorie; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (1, 'Définition Générale', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (2, 'Rendu/Insertion', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (3, 'Accessibilité/Sécurité', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (4, 'Autre', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (5, 'Arrêté', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (6, 'Avis Obligatoires', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (7, 'Document', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (8, 'Daact', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (9, 'Dossier Complet', NULL);
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle, code) VALUES (10, 'Plat''AU', 'PLATAU');


--
-- Data for Name: document_numerise_type; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (29, 'CCO', 'certificat conformité totale lotissement', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (30, 'CCP', 'certificat conformité partielle lotissement', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (31, 'COM', 'passage en commission', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (62, 'INC', 'incomplétude', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (63, 'INCCS', 'incomplétude changement usage', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (64, 'IRA', 'irrecevabilité d''annulation', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (65, 'IRN', 'irrecevabilité de transfert de nom', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (66, 'IRO', 'irrecevabilité de prorogation', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (67, 'IRR', 'irrecevabilité', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (68, 'LIC', 'lettre d''information complémentaire', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (69, 'LMA', 'lettre de mise en attente', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (70, 'MDR', 'mise en demeure de retrait', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (71, 'NDL', 'notification de délai', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (72, 'PDL', 'prolongation de délai', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (73, 'PRL', 'prolongation de delais', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (74, 'RAA', 'rapport d''annulation', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (75, 'RAM', 'rapport modificatif', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (76, 'RAN', 'rapport de transfert de nom', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (77, 'RAO', 'rapport de prorogation', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (78, 'RAP', 'rapport ( ou avis )', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (79, 'RAR', 'rapport de conformité', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (80, 'RDA', 'refus de la déclaration attestant l''achèvement et la conformité des travaux', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (81, 'RIN', 'lettre rappel incomplétude', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (33, 'DGCUB', 'toutes les pièces du certificat d''urbanisme opérationnel', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (34, 'DGIMPA', 'Imprimé de demande de permis d''aménager', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (35, 'DGIMPC', 'Imprimé de demande de permis de construire', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (36, 'DGIMPD', 'Imprimé de demande de permis de démolir', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (37, 'DGPA01', 'plan de situation', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (38, 'DGPA02', 'notice descriptive terrain et projet d''aménagement prévu', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (39, 'DGPA03', 'plan de l''état actuel du terrain à aménager et de ses abords', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (40, 'DGPA04', 'plan de composition d''ensemble du projet', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (41, 'DGPA05', 'vues et coupes du projet dans le profil du terrain naturel', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (42, 'DGPA08', 'programme et plans des travaux d''équipement du lotissement', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (43, 'DGPA10', 'projet de règlement du lotissement', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (44, 'DGPC01', 'plan de situation', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (45, 'DGPC02', 'plan de masse des constructions à édifier ou à modifier', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (46, 'DGPC03', 'plan en coupe du terrain et de la construction', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (47, 'DGPC04', 'notice décrivant le terrain et présentant le projet', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (48, 'DGPC32', 'plan de division du terrain', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (49, 'DGPD01', 'plan de situation', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (50, 'DGPD02', 'plan de masse des constructions à démolir ou à conserver', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (51, 'DGPD03', 'photographie du ou des bâtiments à démolir', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (52, 'DGPD04', 'notice expliquant pourquoi la conservation du bâtiment ne peut plus être assurée (démolition totale)', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (53, 'DGPD05', 'photographies des façades et toitures du bâtiment et des dispositions intérieures (démolition totale)', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (54, 'DGPD06', 'notice expliquant pourquoi la conservation du bâtiment ne peut plus être assurée (démolition partielle)', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (55, 'DGPD07', 'photographies des façades et toitures du bâtiment et de ses dispositions intérieures (démolition partielle)', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (56, 'DGPD08', 'descriptif des moyens mis en oeuvre pour éviter toute atteinte aux parties conservées du bâtiment', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (57, 'DGPD09', 'photographies de l''ensemble des parties extérieures et intérieures du bâtiment adossées à l''immeuble classé', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (58, 'DGPD10', 'descriptif des moyens mis en oeuvre pour éviter toute atteinte à l''immeuble classé', 1, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (82, 'RIPA06', 'photographies du terrain', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (83, 'RIPA07', 'photographie du terrain dans le paysage lointain', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (84, 'RIPA09', 'document graphique des hypothèses d''implantation des bâtiments', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (85, 'RIPA17', 'plan de masse des constructions à édifier ou à modifier', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (86, 'RIPA18', 'plan des façades et des toitures', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (87, 'RIPA19', 'plan en coupe du terrain et de la construction', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (88, 'RIPC05', 'plan des façades et des toitures', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (89, 'RIPC06', 'insertion graphique et photographies du terrain', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (90, 'RIPC07', 'photographie du terrain dans l''environnement proche', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (91, 'RIPC08', 'photographie du terrain dans le paysage lointain', 2, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (12, 'ASPA27', 'étude de sécurité', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (13, 'ASPA49', 'récépissé de dépôt en préfecture de la demande d''autorisation IGH', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (14, 'ASPA50', 'dossier spécifique accessibilité ERP', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (15, 'ASPA51', 'dossier sécurité ERP', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (16, 'ASPC16', 'étude de sécurité', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (17, 'ASPC38', 'récépissé de dépôt en préfecture de la demande d''autorisation IGH', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (18, 'ASPC39', 'dossier spécifique accessibilité ERP', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (19, 'ASPC40', 'dossier sécurité ERP', 3, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (20, 'AUTG', 'toutes les pièces composant le dossier (A0)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (21, 'AUTP', 'toutes les pièces composant le dossier (A3/A4)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (22, 'AUTPAG', 'autres pièces composant le dossier (A0)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (23, 'AUTPAP', 'autres pièces composant le dossier (A3/A4)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (24, 'AUTPCG', 'autres pièces composant le dossier (A0)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (25, 'AUTPCP', 'autres pièces composant le dossier (A3/A4)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (26, 'AUTPDG', 'autres pièces composant le dossier (A0)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (27, 'AUTPDP', 'autres pièces composant le dossier (A3/A4)', 4, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (1, 'APA', 'arrêté participations', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (2, 'ARA', 'arrêté d''annulation', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (3, 'ARD', 'arrêté de différé de travaux', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (4, 'ARE', 'arrêté rectificatif', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (5, 'ARM', 'arrêté modificatif', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (6, 'ARN', 'arrêté de transfert de nom', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (7, 'ARO', 'arrêté de prorogation', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (8, 'ARR', 'arrêté de conformité', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (9, 'ARRT', 'arrêté', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (10, 'ART', 'arrêté retour préfecture', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (11, 'ARV', 'arrêté de vente par anticipation', 5, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (28, 'AVIS', 'avis obligatoires', 6, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (59, 'DOC', 'déclaration d''ouverture de chantier', 7, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (32, 'DAACT', 'déclaration attestant l''achèvement et la conformité des travaux', 8, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (60, 'DOS01', 'autres pièces composant le dossier délivré (A3/A4)', 9, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (61, 'DOS02', 'autres pièces composant le dossier délivré (A0)', 9, true, true, true, NULL, NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (92, '19', 'Attestation de l’aménageur certifiant qu’il a réalisé ou prendra en charge l’intégralité des travaux', 7, true, true, false, 'L’attestation de l’aménageur certifiant qu’il a réalisé ou prendra en charge l’intégralité des travaux mentionnés à l’article R. 331-5 du code de l’urbanisme [Art. R. 431-23-1 du code de l’urbanisme]', NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (93, '111', 'Autre type à préciser', 4, true, true, false, 'Autre type à préciser', NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (94, 'DOCTRAV', 'Document de travail', 4, true, true, false, 'Document de travail', NULL, NULL);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee, description, om_validite_debut, om_validite_fin) VALUES (95, 'PLATAU', 'Test type document numerise de catégorie PLATAU', 10, true, true, true, NULL, NULL, NULL);


--
-- Data for Name: lien_document_n_type_d_i_t; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO lien_document_n_type_d_i_t (lien_document_n_type_d_i_t, document_numerise_type, dossier_instruction_type, code) VALUES (1, 92, 1, 'PC31-1');
INSERT INTO lien_document_n_type_d_i_t (lien_document_n_type_d_i_t, document_numerise_type, dossier_instruction_type, code) VALUES (2, 92, 1, 'F6');
INSERT INTO lien_document_n_type_d_i_t (lien_document_n_type_d_i_t, document_numerise_type, dossier_instruction_type, code) VALUES (3, 92, 16, 'F6');


--
-- Name: document_numerise_nature_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('document_numerise_nature_seq', 3, false);


--
-- Name: document_numerise_type_categorie_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('document_numerise_type_categorie_seq', 11, false);


--
-- Name: document_numerise_type_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('document_numerise_type_seq', 96, false);


--
-- Name: lien_document_n_type_d_i_t_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('lien_document_n_type_d_i_t_seq', 4, false);


--
-- PostgreSQL database dump complete
--

