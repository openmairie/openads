<?php
/**
 * Surcharge de la classe demande afin d'afficher une entrée menu pour 
 * l'affichage du registre réglementaire.
 *
 * @package openfoncier
 * @version SVN : $Id: demande_affichage_reglementaire_registre.inc.php 3356 2015-03-26 14:26:34Z softime $
 */

require_once "../sql/pgsql/demande.inc.php";

// Fil d'ariane
$ent = _("guichet unique")." -> "._("affichage reglementaire")." -> "._("registre");

//
$sousformulaire = array();

//
$tab_title = __("Traitement du registre d'affichage réglementaire");

?>
