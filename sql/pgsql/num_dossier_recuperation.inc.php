<?php
/**
 * Surcharge de autorisation pour afficher directement le formulaire
 * spécifique de synchronisation
 *
 * @package openmarchefor
 * @version SVN : $Id$
 */


// Fil d'Ariane
$ent = __("numerisation") . " -> " . __( "traitement d'un lot" ) . " -> " . __( "récupération du suivi des dossiers" );

//
$tab_title = __("récupération du suivi des dossiers");
