<?php
//$Id$ 
//gen openMairie le 10/09/2020 13:54

$import= "Insertion dans la table departement voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."departement";
$id='departement'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "dep" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "3",
    ),
    "reg" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "2",
    ),
    "cheflieu" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "5",
    ),
    "tncc" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "1",
    ),
    "ncc" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "200",
    ),
    "nccenr" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "200",
    ),
    "libelle" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "45",
    )
);
