<?php
/**
 * Surcharge de la classe instruction afin d'afficher une entrée menu pour 
 * le suivi des envois de lettre RAR.
 * 
 * @package openfoncier
 * @version SVN : $Id$
 */

require_once "../sql/pgsql/instruction.inc.php";

// Fil d'ariane
$ent = _("suivi")." -> "._("suivi des pieces")." -> "._("envoi lettre AR");

//
$sousformulaire = array();

//
$tab_title = _("imprimer les AR");

?>
