<?php
//$Id$ 
//gen openMairie le 20/08/2020 15:54

$import= "Insertion dans la table commune voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."commune";
$id='commune'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "typecom" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "4",
    ),
    "com" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "5",
    ),
    "reg" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "2",
    ),
    "dep" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "3",
    ),
    "arr" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "4",
    ),
    "tncc" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "1",
    ),
    "ncc" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "200",
    ),
    "nccenr" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "200",
    ),
    "libelle" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "45",
    ),
    "can" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "comparent" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    )
);
