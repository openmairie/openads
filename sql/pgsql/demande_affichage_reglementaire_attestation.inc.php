<?php
/**
 * Surcharge de la classe demande afin d'afficher une entrée menu pour 
 * l'affichage d'attestation réglementaire.
 *
 * @package openfoncier
 * @version SVN : $Id: demande_affichage_reglementaire_attestation.inc.php 3356 2015-03-26 14:26:34Z softime $
 */

require_once "../sql/pgsql/demande.inc.php";

// Fil d'ariane
$ent = _("guichet unique")." -> "._("affichage reglementaire")." -> "._("attestation");

//
$sousformulaire = array();

//
$tab_title = _("imprimer l'attestation d'affichage reglementaire");

?>
