<?php
/**
 * Définition des variables communes permettant de définir des éléments de la
 * clause SELECT des requêtes des listings.
 *
 * @package openfoncier
 * @version SVN : $Id$
 */

// Colonne représentant le libellé du dossier : elle se compose du libellé du
// dossier ainsi que d'une classe pour identifier le code DATD du dossier.
// Cette classe CSS permet d'appliquer un style spécifique en fonction du type
// du dossier sur la valeur affichée dans les listings.
// Attention la requête qui utilise ce SELECT doit avoir une jointure vers la
// table *dossier_autorisation_type_detaille*.
$select__dossier_libelle__column = "dossier.dossier_libelle";
if (isset($f) === false) {
    $f = $this->f;
}
if ($f->is_option_afficher_couleur_dossier() === true) {
    $select__dossier_libelle__column = sprintf(
        'CONCAT(
            \'%1$s\',
            dossier.dossier_libelle,
            \'%2$s\'
        )',
        sprintf('<span id="\', %1$s, \'" class="\', %2$s, \'">',
            'dossier.dossier_libelle', 
            'CONCAT(\'datd-\', dossier_autorisation_type_detaille.code)'
        ),
        '</span>'
    );
}
$select__dossier_libelle__column_as = sprintf('%s as "%s"', $select__dossier_libelle__column, __("dossier_libelle"));
