<?php
/**
 * Surcharge de la classe dossier afin d'afficher l'export SITADEL
 * 
 * @package openfoncier
 * @version SVN : $Id: sitadel.inc.php 5044 2015-08-14 16:34:39Z vpihour $
 */
//
require_once "../sql/pgsql/dossier.inc.php";

//
$sousformulaire = array();

$tab_title = __("Export");

?>