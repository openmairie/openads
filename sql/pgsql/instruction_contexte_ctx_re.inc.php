<?php
/**
 * Script de paramétrage du listing des instructions.
 *
 * Dans le contexte d'un dossier contentieux recours. 
 *
 * @package openads
 * @version SVN : $Id: instruction_contexte_ctx_re.inc.php 6565 2017-04-21 16:14:15Z softime $
 */

include('../sql/pgsql/instruction_contexte_ctx.inc.php');

?>