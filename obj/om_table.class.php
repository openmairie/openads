<?php
/**
 * Ce script définit la classe 'om_table'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_table.class.php";

/**
 * Définition de la classe 'om_table' (om_table).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_table' du framework pour des besoins spécifiques de
 * l'application.
 */
class om_table extends table {

    /**
     * Méthode de composition du tri
     *
     * @return string
     */
    function composeTri() {
        $tri = $this->tri;
        if ((string) $this->getParam("tricol") !== ""
            and isset($this->champAffiche[abs((int) $this->getParam("tricol"))])) {

            // Tricol est la cle du tableau de champ, il faut recuperer le
            // champ pour l'integrer dans la requete on verifie si un 'as' est
            // present pour s'en servir
            $tricol = $this->champAffiche[abs((int) $this->getParam("tricol"))];
            $tab = preg_split("/[ )]as|AS /", $tricol);

            if (count($tab) > 1) {

                /* Permet de determiner si la colonne a trier est une date.
                   Si $tab[0] contient DD/MM/YYYY, c'est une date.
                   Fonctionne actuellement qu'avec PostgresSQL. */

                if (strpos($tab[0], "'DD/MM/YYYY'")) {
                    // si c'est une date, on recupere le premier parametre
                    // de la fonction to_char
                    $tricol = str_replace("to_char(", "", $tab[0]);
                    $tricol = trim(str_replace(",'DD/MM/YYYY')", "", $tricol));
                    $tricol = trim(str_replace(", 'DD/MM/YYYY')", "", $tricol));
                } else {
                    // dans le cas d'un champ non-date, on recupere table.colonne
                    $tricol = $tab[0];
                }

                // seul un signe "-" en début de paramètre de tri effectue un tri
                // par ordre décroissant
                if (substr((string) $this->getParam("tricol"), 0, 1) !== "-") {

                    // tri croissant, nulls en dernier
                    if (OM_DB_PHPTYPE == 'pgsql') {
                        $tricol .= " ASC NULLS LAST";
                    } else {
                        $tricol = ' ISNULL('.$tricol.') ASC, '.$tricol.' ASC';
                    }
                } else {

                    // tri decroissant, nulls en dernier
                    if (OM_DB_PHPTYPE == 'pgsql') {
                        $tricol .= " DESC NULLS LAST";
                    } else {
                        $tricol = ' ISNULL('.$tricol.') ASC, '.$tricol.' DESC';
                    }
                }
            }

            // Si $tri n'est pas vide alors il faut inserer le nouveau champ
            // de tri en premier dans le ORDER BY
            if ($tri) {
                $tri = str_replace("order by ", "ORDER BY ".$tricol.",",
                                   strtolower($tri));
            } else {
                $tri = " order by ".$tricol."";
            }

        }
        return $tri;
    }

    /**
     * SURCHARGE
     *
     * Calcule et affiche la ligne d'entête du tableau.
     *
     * @param array $info
     * @param string $class Prefixe de la classe CSS a utiliser
     * @param boolean $onglet
     *
     * @return void
     */
    function displayHeader($info, $class = "tab", $onglet = false) {
        // Renomme le champ collectivité en service si l'option est activée
        if ($this->f->is_option_renommer_collectivite_enabled() === true) {
            foreach ($info as $key => $elem) {
                if ($elem["name"] === strtolower(__("collectivite"))
                    || $elem["name"] === strtolower(__("om_collectivite"))) {
                    //
                    $info[$key]["name"] = strtolower(__("service"));
                }
                if ($elem["name"] === strval(__("collectivite"))
                    || $elem["name"] === strval(__("om_collectivite"))) {
                    //
                    $info[$key]["name"] = strval(__("service"));
                }
            }
        }

        $getValues = $this->f->get_submitted_get_value();
        // Pour les sous-dossier, le clic sur le bouton "ajouter" entraine l'affichage
        // de la page de consultation du sous-dossier en tant que formulaire (sans ajax).
        //
        // Par défaut les actions de type "corner" affichées dans un onglet sont construite
        // pour utiliser un ajaxIt() afin d'accéder au lien paramétré. Pour contourner ce
        // problème si on est dans le contexte de l'onglet "sous-dossier" alors on modifie
        // la valeur du paramètre "onglet" pour faire comme si on était dans le contexte
        // d'un tab au lieu d'un soustab. Ainsi l'action va s'afficher sans ajaxIt et
        // permettre d'afficher un formulaire à la place d'un sous-formulaire.
        if (! empty($getValues) &&
            (! empty($getValues['obj']) &&
                $getValues['obj'] == 'sous_dossier')) {
            $onglet = false;
        }

        parent::displayHeader($info, $class, $onglet);
    }

    /**
     * SURCHARGE
     * 
     * Méthode de composition de la liste des champs du SELECT
     *
     * @return string
     */
    function composeChamp() {
        // Concatenation des champAffiche avec des virgules pour composer la
        // clause SELECT
        $champ = "";
        foreach ($this->champAffiche as $elem) {
            $champ = $champ."".$elem.",";
        }
        $champ = mb_substr($champ, 0, mb_strlen($champ) - 1);
        if ($this->f->is_option_renommer_collectivite_enabled() === true) {
            $champ = str_ireplace(array(__('om_collectivite'), __('collectivite')), __('service'), $champ);
        }
        return $champ;
    }

    /**
     * SURCHARGE
     *
     * Gestion de la recherche.
     *
     * @return void
     */
    function composeSearchTab() {
        parent::composeSearchTab();

        // Renomme le champ collectivité en service si l'option est activée
        if ($this->f->is_option_renommer_collectivite_enabled() === true) {
            if (in_array(__("om_collectivite"), $this->searchTab["display"]) === true) {
                $om_collectivite_keys = array_keys($this->searchTab["display"], __("om_collectivite"));
                foreach ($om_collectivite_keys as $om_collectivite_key) {
                    $this->searchTab["display"][$om_collectivite_key] = __("service");
                }
            }
            if (in_array(__("collectivite"), $this->searchTab["display"]) === true) {
                $om_collectivite_keys = array_keys($this->searchTab["display"], __("collectivite"));
                foreach ($om_collectivite_keys as $om_collectivite_key) {
                    $this->searchTab["display"][$om_collectivite_key] = __("service");
                }
            }
        }
    }

   /**
     * SURCHARGE/OVERLOAD
     *
     * Affichage principal.
     *
     * Surcharge pour retirer les espaces non sécables des icones (&nbsp;)
     * via capture du buffer d'output.
     *
     * @param array $params
     * @param array $actions
     * @param mixed $db
     * @param string $class Prefixe de la classe CSS a utiliser
     * @param boolean $onglet
     *
     * @return void
     */
    public function display($params = array(), $actions = array(), $db = NULL, $class = "tab", $onglet = false) {
        ob_start();
        parent::display($params, $actions, $db, $class, $onglet);
        $out = ob_get_contents();
        ob_end_clean();
        echo str_replace(
            array('</a>&nbsp;</td>', '</a>&nbsp;<a', '<td class="icons">&nbsp;'),
            array('</a></td>', '</a><a', '<td class="icons">'),
            $out
        );
    }

}
