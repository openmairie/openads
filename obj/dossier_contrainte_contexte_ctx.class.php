<?php
/**
 * DBFORM - 'dossier_contrainte_contexte_ctx' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id: dossier_contrainte_contexte_ctx.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once ("../obj/dossier_contrainte.class.php");

class dossier_contrainte_contexte_ctx extends dossier_contrainte {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_contrainte_contexte_ctx";

}


