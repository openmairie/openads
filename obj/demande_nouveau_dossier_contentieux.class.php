<?php
/**
 * DBFORM - 'demande_nouveau_dossier_contentieux' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_nouveau_dossier_contentieux'.
 *
 * @package openads
 * @version SVN : $Id: demande_nouveau_dossier_contentieux.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once "../obj/demande_nouveau_dossier.class.php";

/**
 * Définition de la classe 'demande_nouveau_dossier'.
 *
 * Cette classe permet d'interfacer l'ajout de demande concernant un nouveau
 * dossier depuis le menu Contentieux > Nouveau dossier.
 */
class demande_nouveau_dossier_contentieux extends demande_nouveau_dossier {

    /**
     *
     */
    protected $_absolute_class_name = "demande_nouveau_dossier_contentieux";

}


