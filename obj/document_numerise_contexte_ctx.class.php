<?php
/**
 * DBFORM - 'document_numerise_contexte_ctx' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id: document_numerise_contexte_ctx.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once ("../obj/document_numerise.class.php");

class document_numerise_contexte_ctx extends document_numerise {

    /**
     *
     */
    protected $_absolute_class_name = "document_numerise_contexte_ctx";

}


