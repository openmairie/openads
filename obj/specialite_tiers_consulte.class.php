<?php
//$Id$ 
//gen openMairie le 03/03/2022 13:05

require_once "../gen/obj/specialite_tiers_consulte.class.php";

class specialite_tiers_consulte extends specialite_tiers_consulte_gen {

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "specialite_tiers_consulte",
            "code",
            "libelle",
            "description",
            "om_validite_debut",
            "om_validite_fin",
        );
    }

}
