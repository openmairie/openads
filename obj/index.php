<?php
/**
 * Ce fichier permet de faire une redirection vers le fichier index.php a la
 * racine de l'application.
 *
 * @package openmairie_exemple
 * @version SVN : $Id: index.php 4418 2015-02-24 17:30:28Z tbenita $
 */

header("Location: ../index.php");
exit();


