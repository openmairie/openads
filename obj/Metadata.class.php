<?php

/**
 * Représente un ensemble de métadonnées d'un document dans openADS
 */
class Metadata
{
    /**
     * Nom du document
     * @required
     * @var $name
     */
    public $name;

    /**
     * Type MIME du document
     * @required
     * @var $mimetype
     */
    public $mimetype;

    /**
     * Taille du document (en octets)
     * @required
     * @var $size
     */
    public $size;
}
