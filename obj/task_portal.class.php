<?php
/**
 * DBFORM - 'task_portal' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'task_portal'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/task.class.php";

class task_portal extends task {

    /**
     * Catégorie de la tâche
     */
    var $category = PORTAL;

    /**
     *
     */
    protected $_absolute_class_name = "task_portal";

}
