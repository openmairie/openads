<?php
/**
 * DBFORM - 'dossier_non_transmis' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/dossier_instruction.class.php";

//
class dossier_non_transmis extends dossier_instruction {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_non_transmis";

}