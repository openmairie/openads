<?php
/**
 * DBFORM - 'donnees_techniques_contexte_ctx' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id: donnees_techniques_contexte_ctx.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once ("../obj/donnees_techniques.class.php");

class donnees_techniques_contexte_ctx extends donnees_techniques {

    /**
     *
     */
    protected $_absolute_class_name = "donnees_techniques_contexte_ctx";

}


