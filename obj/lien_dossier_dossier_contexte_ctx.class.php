<?php
/**
 * DBFORM - 'lien_dossier_dossier_contexte_ctx' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id: lien_dossier_dossier_contexte_ctx.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once ("../obj/lien_dossier_dossier.class.php");

class lien_dossier_dossier_contexte_ctx extends lien_dossier_dossier {

    /**
     *
     */
    protected $_absolute_class_name = "lien_dossier_dossier_contexte_ctx";

}


