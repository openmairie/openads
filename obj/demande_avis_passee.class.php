<?php
/**
 * DBFORM - 'demande_avis_passee' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_avis_passee'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/demande_avis.class.php";

class demande_avis_passee extends demande_avis {

    /**
     *
     */
    protected $_absolute_class_name = "demande_avis_passee";

}


