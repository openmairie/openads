<?php
/**
 * DBFORM - 'instruction_suivi_bordereaux' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'instruction_suivi_bordereaux'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/instruction.class.php";

/**
 * Surcharge de la classe instruction afin d'afficher une entrée menu pour 
 * le suivi des bordereaux.
 */
class instruction_suivi_bordereaux extends instruction {

    /**
     *
     */
    protected $_absolute_class_name = "instruction_suivi_bordereaux";

}


