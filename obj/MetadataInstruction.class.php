<?php

require_once __DIR__.'/Metadata.class.php';

/**
 * Réprésente les métadonnées d'un document d'instruction
 */
class MetadataInstruction extends Metadata
{
    // om_fichier_instruction
    /**
     * @var string
     */
    public $dossier;
    /**
     * @var string
     */
    public $dossier_version;
    /**
     * @var string
     */
    public $numDemandeAutor;
    /**
     * @var string
     */
    public $anneemoisDemandeAutor;
    /**
     * @var string
     */
    public $typeInstruction;
    /**
     * @var string
     */
    public $statutAutorisation;
    /**
     * @var string
     */
    public $typeAutorisation;
    /**
     * @var string
     */
    public $dateEvenementDocument;
    /**
     * @var string
     */
    public $groupeInstruction;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $concerneERP;

    // arrêté
    /**
     * @var string
     */
    public $numArrete;
    /**
     * @var string
     */
    public $ReglementaireArrete;
    /**
     * @var string
     */
    public $NotificationArrete;
    /**
     * @var string
     */
    public $dateNotificationArrete;
    /**
     * @var string
     */
    public $controleLegalite;
    /**
     * @var string
     */
    public $dateSignature;
    /**
     * @var string
     */
    public $nomSignataire;
    /**
     * @var string
     */
    public $qualiteSignataire;
    /**
     * @var string
     */
    public $ap_numRue;
    /**
     * @var string
     */
    public $ap_nomDeLaVoie;
    /**
     * @var string
     */
    public $ap_codePostal;
    /**
     * @var string
     */
    public $ap_ville;
    /**
     * @var string
     */
    public $activite;
    /**
     * @var string
     */
    public $dateControleLegalite;
}
