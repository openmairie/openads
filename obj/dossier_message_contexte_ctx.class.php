<?php
/**
 * DBFORM - 'dossier_message_contexte_ctx' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id: dossier_message_contexte_ctx.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once ("../obj/dossier_message.class.php");

class dossier_message_contexte_ctx extends dossier_message {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_message_contexte_ctx";

}


