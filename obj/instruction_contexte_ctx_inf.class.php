<?php
/**
 * DBFORM - 'instruction_contexte_ctx_inf' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id: instruction_contexte_ctx_inf.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once ("../obj/instruction_contexte_ctx.class.php");

class instruction_contexte_ctx_inf extends instruction_contexte_ctx {

    /**
     *
     */
    protected $_absolute_class_name = "instruction_contexte_ctx_inf";

}


