<?php
/**
 * DBFORM - 'dossier_contentieux_toutes_infractions' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/dossier_contentieux.class.php";

//
class dossier_contentieux_toutes_infractions extends dossier_contentieux {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_contentieux_toutes_infractions";

}


