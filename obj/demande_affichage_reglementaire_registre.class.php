<?php
/**
 * DBFORM - 'demande_affichage_reglementaire_registre' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_affichage_reglementaire_registre'.
 *
 * @package openads
 * @version SVN : $Id: demande_affichage_reglementaire_registre.class.php 3356 2015-03-26 14:26:34Z softime $
 */

require_once "../obj/demande.class.php";

/**
 * Définition de la classe 'demande_affichage_reglementaire_registre'.
 *
 * Surcharge de la classe demande afin d'afficher une entrée menu pour
 * l'affichage du registre réglementaire.
 */
class demande_affichage_reglementaire_registre extends demande {

    /**
     *
     */
    protected $_absolute_class_name = "demande_affichage_reglementaire_registre";

}


