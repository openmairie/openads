<?php
/**
 * DBFORM - 'document_instruction' - Surcharge obj.
 *
 * @package openfoncier
 * @version SVN : $Id$
 */

//
require_once "../obj/instruction.class.php";

//
class document_instruction extends instruction {

    /**
     *
     */
    protected $_absolute_class_name = "document_instruction";
    
}// fin classe
