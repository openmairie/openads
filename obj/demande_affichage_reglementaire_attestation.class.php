<?php
/**
 * DBFORM - 'demande_affichage_reglementaire_attestation' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_affichage_reglementaire_attestation'.
 *
 * @package openads
 * @version SVN : $Id: demande_affichage_reglementaire_attestation.class.php 3356 2015-03-26 14:26:34Z softime $
 */

require_once "../obj/demande.class.php";

/**
 * Définition de la classe 'demande_affichage_reglementaire_attestation'.
 *
 * Surcharge de la classe demande afin d'afficher une entrée menu pour
 * l'affichage d'attestation réglementaire.
 */
class demande_affichage_reglementaire_attestation extends demande {

    /**
     *
     */
    protected $_absolute_class_name = "demande_affichage_reglementaire_attestation";

}


