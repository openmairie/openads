<?php
/**
 * DBFORM - 'demande_autre_dossier' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_autre_dossier'.
 *
 * @package openads
 * @version SVN : $Id: demande_autre_dossier.class.php 5984 2016-02-19 08:41:12Z fmichon $
 */

require_once "../obj/demande_dossier_encours.class.php";

/**
 * Définition de la classe 'demande_autre_dossier'.
 *
 * Cette classe permet d'interfacer l'ajout de demande sur un dossier existant.
 */
class demande_autre_dossier extends demande_dossier_encours {

    /**
     *
     */
    protected $_absolute_class_name = "demande_autre_dossier";

}


