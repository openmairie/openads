<?php
/**
 * DBFORM - 'dossier_contentieux_tous_recours' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'dossier_contentieux_tous_recours'.
 *
 * @package openads
 * @version SVN : $Id$
 */

//
require_once "../obj/dossier_contentieux.class.php";

//
class dossier_contentieux_tous_recours extends dossier_contentieux {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_contentieux_tous_recours";

}


