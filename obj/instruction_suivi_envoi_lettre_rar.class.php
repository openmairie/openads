<?php
/**
 * DBFORM - 'instruction_suivi_envoi_lettre_rar' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'instruction_suivi_envoi_lettre_rar'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/instruction.class.php";

/**
 * Surcharge de la classe instruction afin d'afficher une entrée menu pour 
 * le suivi des envois de lettre RAR.
 */
class instruction_suivi_envoi_lettre_rar extends instruction {

    /**
     *
     */
    protected $_absolute_class_name = "instruction_suivi_envoi_lettre_rar";

}


