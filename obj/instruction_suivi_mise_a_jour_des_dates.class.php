<?php
/**
 * DBFORM - 'instruction_suivi_mise_a_jour_des_dates' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'instruction_suivi_mise_a_jour_des_dates'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/instruction.class.php";

/**
 * Surcharge de la classe instruction afin d'afficher une entrée menu pour 
 * la mise à jour des dates de l'instruction.
 */
class instruction_suivi_mise_a_jour_des_dates extends instruction {

    /**
     *
     */
    protected $_absolute_class_name = "instruction_suivi_mise_a_jour_des_dates";

}


