<?php
/**
 * DBFORM - 'blocnote_contexte_ctx' - Surcharge obj.
 *
 * @package openads
 * @version SVN : $Id: blocnote_contexte_ctx.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once ("../obj/blocnote.class.php");

/**
 * Surcharge de la classe 'blocnote' pour gérer l'affichage de l'onglet selon le contexte.
 */
class blocnote_contexte_ctx extends blocnote {

    /**
     *
     */
    protected $_absolute_class_name = "blocnote_contexte_ctx";

}


