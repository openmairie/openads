<?php
/**
 * DBFORM - 'sitadel' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'sitadel'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/dossier.class.php";

class sitadel extends dossier {

    /**
     *
     */
    protected $_absolute_class_name = "sitadel";

    /**
     * Permet de modifier le fil d'Ariane
     * @param string $ent Fil d'Ariane
     * @param array  $val Valeurs de l'objet
     * @param intger $maj Mode du formulaire
     */
    function getFormTitle($ent) {
        // Change le fil d'Ariane
        return _("export / import")." -> "._("export SITADEL");
    }

}


