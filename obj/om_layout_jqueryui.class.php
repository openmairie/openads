<?php
/**
 * Ce script contient la définition de la classe SURCHARGE 'om_layout_jqueryui'.
 *
 * @package openads
 * @version SVN : $Id$
 */

//
require_once PATH_OPENMAIRIE."om_layout_jqueryui.class.php";

/**
 * Définition de la classe SURCHARGE 'om_layout_jqueryui'.
 */
class om_layout_jqueryui extends layout_jqueryui {

    /**
     * SURCHARGE
     *
     * Affiche le ou les logos :
     * Le logo spécifique si celui-ci existe, et le logo de l'application dans
     * tous les cas.
     *
     * @return void
     */
    public function display_logo() {
        // Le fichier contenant l'image du logo spécifique doit se nommer
        // "logo_customer.jpg" ou "logo_customer.png" et être stocké dans le
        // répertoire "app/img/" à la racine de l'application.
        $filename = null;
        if (file_exists(PATH_OPENMAIRIE."../app/img/logo_customer.jpg") === true) {
            $filename = PATH_OPENMAIRIE."../app/img/logo_customer.jpg";
        }
        if (file_exists(PATH_OPENMAIRIE."../app/img/logo_customer.png") === true) {
            $filename = PATH_OPENMAIRIE."../app/img/logo_customer.png";
        }
        // Calcul de la longueur du logo spécifique en se basant sur une
        // hauteur fixe à 50px.
        $width = null;
        if ($filename !== null) {
            $image_size = getimagesize($filename);
            if ($image_size !== false) {
                $width = round(50*($image_size[0]/$image_size[1]));
            }
        }
        // Affiche seulement le logo de l'application si le logo spécifique
        // n'existe pas ou qu'il ne s'agit pas d'une image valide.
        if ($filename !== null && $width !== null) {
            // Logo
            echo "\t<div id=\"logo\">\n";
            //
            echo "\t\t<h1>\n";
            // Lien vers le tableau de bord
            echo "\t\t<a class=\"logo_customer\" style=\"display: inline-block;\"";
            echo "href=\"".$this->get_parameter("url_dashboard")."\" ";
            echo "title=\"".__("Tableau de bord")."\">\n";
            //
            echo "\t\t\t<span class=\"logo_customer\" style=\"height: 50px; width: ".$width."px;\" >";
            echo "logo_customer";
            echo "</span>\n";
            //
            echo "\t\t</a>\n";
            // Lien vers le tableau de bord
            echo "\t\t<a class=\"logo\" style=\"display: inline-block;\"";
            echo "href=\"".$this->get_parameter("url_dashboard")."\" ";
            echo "title=\"".__("Tableau de bord")."\">\n";
            //
            echo "\t\t\t<span class=\"logo\">";
            echo $this->get_parameter("application");
            echo "</span>\n";
            //
            echo "\t\t</a>\n";
            //
            echo "\t\t</h1>\n";
            // Fin du logo
            echo "\t</div>\n";
        } else {
            parent::display_logo();
        }
    }

}
