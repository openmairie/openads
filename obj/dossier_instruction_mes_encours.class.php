<?php
/**
 * DBFORM - 'dossier_instruction_mes_encours' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'dossier_instruction_mes_encours'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/dossier_instruction.class.php";

class dossier_instruction_mes_encours extends dossier_instruction {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_instruction_mes_encours";

}


