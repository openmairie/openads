<?php
/**
 * DBFORM - 'dossier_instruction_tous_clotures' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'dossier_instruction_tous_clotures'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/dossier_instruction.class.php";

class dossier_instruction_tous_clotures extends dossier_instruction {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_instruction_tous_clotures";

}


