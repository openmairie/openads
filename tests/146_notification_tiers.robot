*** Settings ***
Documentation     Notification des demandeurs

# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown

*** Test Cases ***
Constitution du jeu de données
    [Documentation]  constitution d'un jeu de données servant à tester le bon fonctionnement
    ...  de la notification des demandeurs

    Depuis la page d'accueil  admin  admin
    # Enregistrement de la date pour la saisie des formulaire
    ${date_courante} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Set Suite Variable  ${date_courante}

    # CONTEXTE
    Set Suite Variable  ${acteur}  notifTiers
    Set Suite Variable  ${collectivite}  LIBRECOM_NOTIFTIERS
    &{librecom_values} =  Create Dictionary
    ...  om_collectivite_libelle=${collectivite}
    ...  departement=22
    ...  commune=002
    ...  insee=22002
    ...  direction_code=Notif
    ...  direction_libelle=Direction de ${collectivite}
    ...  direction_chef=Chef
    ...  division_code=NotifT
    ...  division_libelle=Division NotifT
    ...  division_chef=Chef
    ...  guichet_om_utilisateur_nom=NotificationTiers Guichet
    ...  guichet_om_utilisateur_email=ntguichet@openads-test.fr
    ...  guichet_om_utilisateur_login=ntguichet
    ...  guichet_om_utilisateur_pwd=ntguichet
    ...  instr_om_utilisateur_nom=NotificationTiers Instr
    ...  instr_om_utilisateur_email=ninstr@openads-test.fr
    ...  instr_om_utilisateur_login=ninstr
    ...  instr_om_utilisateur_pwd=ninstr
    ...  acteur=${acteur}
    Isolation d'un contexte  ${librecom_values}

    # PARAMETRAGE
    # Activation de l'option option_dossier_commune et de l'option option_mode_service_consulte
    &{om_param} =  Create Dictionary
    ...  libelle=option_dossier_commune
    ...  valeur=true
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}
    Activer le mode service consulté
    # ajoute le paramètre 'acteur' à la collectivité/au service
    &{om_param} =  Create Dictionary
    ...  libelle=platau_acteur_service_consulte
    ...  valeur=${acteur}
    ...  om_collectivite=${collectivite}
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}
    # définir les paramètres de type de demande
    &{platau_type_demande_initial} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_PCI
    ...  valeur=DI
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${platau_type_demande_initial}
    # paramètrage du titre et du message de notification
    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_tiers_type_titre
    ...  valeur=[openADS] Notification pour les tiers concernant le dossier (avec un caractère accentué) (avec un caractère accentué) [DOSSIER]
    ...  om_collectivite=${collectivite}
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_tiers_type_message
    ...  valeur=Bonjour les tiers (avec un caractère accentué), veuillez prendre connaissance du(des) document(s) suivant(s) :<br> [LIEN_TELECHARGEMENT_DOCUMENT]<br>[LIEN_TELECHARGEMENT_ANNEXE]
    ...  om_collectivite=${collectivite}
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=parametre_notification_url_acces
    ...  valeur=http://localhost/openads/
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}
   

    # PARAMÉTRAGE NOTIF AUTO
    # On ajoute 2 communes : 1 lié au dossier l'autre non
    Set Suite Variable  ${lib_commune}  NTC
    Set Suite Variable  ${code_commune}  ${librecom_values.commune}
    ${code_dept} =  Set Variable    ${librecom_values.departement}
    &{com_values} =  Create Dictionary
    ...  typecom=COM
    ...  com=${code_commune}
    ...  reg=20
    ...  dep=${code_dept}
    ...  arr=100
    ...  tncc=0
    ...  ncc=NOTIFATC
    ...  nccenr=NOTIFATC
    ...  libelle=${lib_commune}
    ...  can=20
    ...  om_validite_debut=23/11/2020
    Ajouter commune avec dates validité  ${com_values}
    ${lib_commune_2} =  Set Variable  NTC2
    ${code_commune_2} =  Set Variable  22003
    &{com_values} =  Create Dictionary
    ...  typecom=COM
    ...  com=${code_commune_2}
    ...  reg=20
    ...  dep=${code_dept}
    ...  arr=200
    ...  tncc=0
    ...  ncc=NOTIFFTC
    ...  nccenr=NOTIFFTC
    ...  libelle=${lib_commune_2}
    ...  can=20
    ...  om_validite_debut=${date_courante}
    Ajouter commune avec dates validité  ${com_values}

    # On ajoute 2 départements
    #  - 1 associé à la commune du dossier
    #  - 1 non associé au dossier
    ${lib_dept} =  Set Variable  Dept NTC
    &{dept_values} =  Create Dictionary
    ...  dep=${code_dept}
    ...  reg=20
    ...  cheflieu=20001
    ...  tncc=0
    ...  ncc=DEPT1
    ...  nccenr=DEPT1NOT
    ...  libelle=${lib_dept}
    ...  om_validite_debut=${date_courante}
    Ajouter département  ${dept_values}
    ${lib_dept_2} =  Set Variable  Dept NTC2
    ${code_dept_2} =  Set Variable  23
    &{dept_values} =  Create Dictionary
    ...  dep=${code_dept_2}
    ...  reg=23
    ...  cheflieu=23000
    ...  tncc=1
    ...  ncc=DEPT2
    ...  nccenr=DEPT2NOT
    ...  libelle=${lib_dept_2}
    ...  om_validite_debut=${date_courante}
    Ajouter département  ${dept_values}

    # On ajoute une catégorie de tiers consulté
    ${om_collectivite_tiers} =  Create List  ${librecom_values.om_collectivite_libelle}
    ${cat_tiers} =  Set Variable  Categorie test notif tiers
    &{categorie_tiers} =  Create Dictionary
    ...  code=CAT_TEST_NTC
    ...  libelle=${cat_tiers}
    ...  description=Categorie servant pour le test de la notification des tiers consultés.
    ...  om_collectivite=${om_collectivite_tiers}
    Ajouter la categorie de tiers consulte  ${categorie_tiers}

    # tiers consulté
    Set Suite Variable  ${tcnotif}  Tiers consulté notifiable
    Set Suite Variable  ${liste_diffusion_tc}  tcnotifiable@atreal.fr
    &{tc_values} =  Create Dictionary
    ...  categorie_tiers_consulte=${cat_tiers}
    ...  abrege=TCN
    ...  liste_diffusion=${liste_diffusion_tc}
    ...  libelle=${tcnotif}
    ...  ville=${collectivite}
    ...  accepte_notification_email=true
    Ajouter le tiers consulte depuis le listing  ${tc_values}

    # types d'habilitation de tiers consulté
    ${type_habilitation_ok} =  Set Variable  TYPE HAB NOTIFIABLE
    &{type_habilitation_tiers_consulte} =  Create Dictionary
    ...  code=457
    ...  libelle=${type_habilitation_ok}
    ...  om_validite_debut=${date_courante}
    Ajouter un type d'habilitation de tiers consulté  ${type_habilitation_tiers_consulte}

    # habilitations 
    @{communes} =  Create List  ${code_commune} - ${lib_commune}  ${code_commune_2} - ${lib_commune_2}
    @{depts} =  Create List  ${code_dept} - ${lib_dept}  ${code_dept_2} - ${lib_dept_2}
    &{habilitation_tiers_consulte_values} =  Create Dictionary
    ...  type_habilitation_tiers_consulte=${type_habilitation_ok}
    ...  tiers_consulte=${tcnotif}
    Ajouter une habilitation de tiers consulté  ${habilitation_tiers_consulte_values}  ${depts}  ${communes}

    # PARAMETRAGE EVENEMENTS
    &{args_lettretype} =  Create Dictionary
    ...  id=test_NOTIF_tiers
    ...  libelle=Test
    ...  sql=Aucune REQUÊTE
    ...  titre=&idx, &destinataire, aujourdhui&aujourdhui, datecourrier&datecourrier, &departement
    ...  corps=Ceci est un document
    ...  actif=true
    ...  collectivite=agglo
    Ajouter la lettre-type depuis le menu  &{args_lettretype}
    # On ajoute un événement avec notification automatique des tiers consulté
    # et on test l'affichage du champs de sélection des types d'habilitation
    @{etat_source} =  Create List  delai de notification envoye
    @{type_di} =  Create List  PCI - P - Initial
    @{type_habilitation_tiers_consulte} =  Create List  ${type_habilitation_ok}
    Set Suite Variable  ${evenement_notif_auto_tc}  TEST_NOTIF_TC
    &{args_evenement} =  Create Dictionary
    ...  libelle=${evenement_notif_auto_tc}
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF_tiers Test
    ...  notification_tiers=Notification automatique
    ...  type_habilitation_tiers_consulte=${type_habilitation_tiers_consulte}
    Ajouter l'événement depuis le menu  ${args_evenement}

Activation de la notification par mail
    [Documentation]  Active la notification par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    &{om_param} =  Create Dictionary
    ...  libelle=option_notification
    ...  valeur=mail
    ...  om_collectivite=${collectivite}
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    Démarrer maildump

TNR doublon notification tiers consulté
    [Documentation]  Test de la notification automatique des tiers consultés.
    ...  Vérifie que seul les tiers ayant le type d'habilitation sélectionné
    ...  sont notifiés. Vérifie également que le service consulté en charge du
    ...  dossier n'est pas notifié et qu'uniquement les tiers associés à la
    ...  commune et au département du dossier le sont.

    # Préparation de la consultation entrante du dossier
    # Change le type affichage du type de DA
    &{args_da_type} =  Create Dictionary
    ...  affichage_form=CONSULTATION ENTRANTE
    Modifier le type de dossier d'autorisation  Permis de construire  ${args_da_type}
    # Récupère le payload de création DI
    ${json_payload} =  Get File  ${EXECDIR}${/}binary_files${/}json_payload_ref.txt
    ${json_payload} =  Replace String  ${json_payload}  3XY-DK4-7X  000-AAA-00
    ${json_payload} =  Replace String  ${json_payload}  7XY-DK8-5X  AAA-000-00
    ${json_payload} =  Replace String  ${json_payload}  13055  ${code_commune}
    ${json_payload} =  Replace String  ${json_payload}  SDF-ZER-74R  ${acteur}
    ${json_payload} =  Replace String  ${json_payload}  EF-DSQ-4512  ${acteur}
    ${payload_dict} =  To Json  ${json_payload}
    # Préparation de la tâche de création du dossier
    ${task_values} =  Create Dictionary
    ...  type=create_DI_for_consultation
    ...  json_payload=${json_payload}
    Ajouter la tâche par WS  ${task_values}

    # Ajout du dossier sur lequel on va déclencher la notification automatique
    # associé à la commune et à la consultation entrante voulu
    ${msg} =  Déclencher le traitement des tâches par WS
    # Récupération du numéro de dossier
    ${search_values} =  Create Dictionary
    ...  source_depot=Plat'AU
    ...  om_collectivite=${collectivite}
    Depuis le contexte du dossier d'instruction par la recherche avance  ${search_values}  ${collectivite}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=#dossier_libelle
    ${di_notif_auto_tiers} =  Get Text  css=#dossier_libelle

    # Ajout de l'instruction declenchant la notification et saisie de la date de retour
    # de signature pour déclencher la notification des tiers
    ${inst_notif_auto} =  Ajouter une instruction au DI et la finaliser  ${di_notif_auto_tiers}  ${evenement_notif_auto_tc}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_courante}
    Click On Submit Button In Subform

    Wait until element contains  css=tr:nth-child(1) td[data-column-id="émetteur"]      admin (Administrateur) (automatique)
    Page Should Contain Element  xpath=//tr/td[normalize-space(text()) = "${tcnotif} : ${liste_diffusion_tc}"]  None  INFO  1

    # Réinitialise le type affichage du type de DA
    &{args_da_type} =  Create Dictionary
    ...  affichage_form=ADS
    Modifier le type de dossier d'autorisation  Permis de construire  ${args_da_type}

Rétablissement des paramètres


    # Désactivation de l'option option_dossier_commune et de l'option option_mode_service_consulte
    &{param_args} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=option_dossier_commune
    ...  click_value=agglo
    Supprimer le paramètre (surcharge)  ${param_args}
    Désactiver le mode service consulté
    # supprime le paramètre 'acteur' de la collectivité/au service
    &{param_args} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=platau_acteur_service_consulte
    ...  click_value=${collectivite}
    Supprimer le paramètre (surcharge)  ${param_args}
    # définir les paramètres de type de demande
    &{param_args} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=platau_type_demande_initial_PCI
    ...  click_value=agglo
    Supprimer le paramètre (surcharge)  ${param_args}
