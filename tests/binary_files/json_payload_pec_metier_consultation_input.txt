{
    "external_uids":
    {
        "consultation": "AAA-BBB-CCC",
        "dossier": "RRR-SSS-TTT",
        "pec_metier": "OOO-PPP-QQQ"
    },
    "pec_metier":
    {
        "date_limite_prescrire": "",
        "date_limite_reponse": "2022-08-13",
        "date_pec_metier": "2022-06-14",
        "date_reception": "2022-06-13",
        "service_consulte": "UUU-VVV-WWW",
        "statut_pec_metier": "2",
        "texte_observations": "il manque des pieces.",
        "types_pieces_manquantes":
        [
            "XXX",
            "YYY"
        ]
    }
}