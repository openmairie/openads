<?php
/**
 * Ce fichier permet de configurer le stockage des fichiers sur le filesystem
 *
 * @package openmairie_exemple
 * @version SVN : $Id: filestorage_1.inc.php 6272 2017-03-03 15:27:53Z softime $
 */

/**
 *
 */
$filestorage = array();


$filestorage["filestorage-default"] = array (
    "storage" => "filesystem", // l'attribut storage est obligatoire
    "path" => "../var/filestorage/", // le repertoire de stockage
    "temporary" => array(
        "storage" => "filesystem", // l'attribut storage est obligatoire
        "path" => "../var/tmp/", // le repertoire de stockage
    ),
    "metadata_handlers" => array( // Liste des méthodes à exécuter par table, pour le traitement des métadonnées
    ),
);

?>
