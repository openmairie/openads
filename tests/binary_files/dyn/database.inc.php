<?php
/**
 * Ce script permet de définir une configuration de base de données pour les tests.
 *
 * @package openads
 * @version SVN : $Id$
 */

//
$conn = array(
    1 => array(
        "openADS", // Titre
        "pgsql", // Type de base
        "pgsql", // Type de base
        "postgres", // Login
        "postgres", // Mot de passe
        "tcp", // Protocole de connexion
        "localhost", // Nom d'hote
        "5432", // Port du serveur
        "", // Socket
        "openads", // nom de la base
        "AAAA-MM-JJ", // Format de la date
        "openads", // Nom du schéma
        "", // Préfixe
        "ldap-test", // Paramétrage pour l'annuaire LDAP
        "mail-test", // Paramétrage pour le serveur de mail
        "filestorage-default", // Paramétrage pour le stockage des fichiers
    ),
);

?>
