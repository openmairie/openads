<?php
/**
 * Contient les paramètres de connexion aux réferentiels.
 *
 * // SERVICES_PASS_AUTH
 *
 * @package openads
 * @version SVN : $Id$
 */

/**
 * services/ WS credentials
 */
$services_login = "admin";
$services_password = "admin";

/**
 * Référentiel ERP
 */
// L'URL du référentiel ERP qui reçoit les messages sortants
$ERP_URL_MESSAGES = 'http://localhost/openads/tests_services/rest_entry.php/referentiel_erp_test/';
$STAMP_WS_URL_ = 'http://localhost/openads/tests_services/rest_entry.php/trouillotage_service_test/';
?>
