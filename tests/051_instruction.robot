*** Settings ***
Documentation  Test des événements d'instruction.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Ne pas mettre à jour le dossier d'instruction lors du suivi des dates d'une instruction
    [Documentation]  Lors de la modification d'une instruction, si celle-ci est liée à une
    ...  action utilisant [date_evenement] dans ses règles, alors celles-ci sont exécutées
    ...  sur le dossier d'instruction.
    ...  Lors du suivi des dates de l'instruction, il ne faut pas que le dossier
    ...  d'instruction soit mis à jour quelque soit l'action de l'instruction.

    Depuis la page d'accueil  admin  admin

    # Ajout le WF nécessaire pour le test
    &{args_action} =  Create Dictionary
    ...  action=test051_A001
    ...  libelle=test051_A001
    ...  regle_date_limite=date_evenement
    Ajouter l'action depuis le menu  ${args_action}
    @{etat_source} =  Create List
    ...  delai de notification envoye
    @{type_di} =  Create List
    ...  PCI - P - Initial
    &{args_evenement_001} =  Create Dictionary
    ...  libelle=test051_E001
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  action=${args_action.libelle}
    ...  lettretype=arrete ARRETE
    Ajouter l'événement depuis le menu  ${args_evenement_001}
    &{args_evenement_002} =  Create Dictionary
    ...  libelle=test051_E002
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  action=${args_action.libelle}
    ...  lettretype=arrete ARRETE
    Ajouter l'événement depuis le menu  ${args_evenement_002}

    # Depuis le dossier d'instruction, on deux instructeurs utilisant les deux événements identiques,
    # à chaque ajout on vérifie que la date limite est bien modifiée comme le stipule l'action
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Ménard
    ...  particulier_prenom=Susanne
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${date_evenement_001} =  Add Time To Date  ${date_ddmmyyyy}  1 days  %d/%m/%Y  True  %d/%m/%Y
    Ajouter une instruction au DI et la finaliser  ${di}  ${args_evenement_001.libelle}  ${date_evenement_001}
    Depuis le contexte du dossier d'instruction  ${di}
    Form Static Value Should Be  css=#date_limite  ${date_evenement_001}
    ${date_evenement_002} =  Add Time To Date  ${date_ddmmyyyy}  10 days  %d/%m/%Y  True  %d/%m/%Y
    Ajouter une instruction au DI et la finaliser  ${di}  ${args_evenement_002.libelle}  ${date_evenement_002}
    Depuis le contexte du dossier d'instruction  ${di}
    Form Static Value Should Be  css=#date_limite  ${date_evenement_002}

    # La modification du suivi des dates de la première instruction ajoutée ne devrait pas modifier
    # la date limite du dossier d'instruction
    &{args_instruction} =  Create Dictionary
    ...  date_envoi_signature=${DATE_FORMAT_DD/MM/YYYY}
    Modifier le suivi des dates  ${di}  ${args_evenement_001.libelle}  ${args_instruction}
    Depuis le contexte du dossier d'instruction  ${di}
    Form Static Value Should Be  css=#date_limite  ${date_evenement_002}
