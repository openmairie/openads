<?php
/**
 * Ce script contient la définition de la classe 'ImportSpecificPHP70Test'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "testImportSpecific_common.php";
final class ImportSpecificPHP70Test extends ImportSpecificCommon {
    public function setUp() {
        $this->common_setUp();
    }
    public function tearDown() {
        $this->common_tearDown();
    }
    public function onNotSuccessfulTest(Throwable $e) {
        $this->common_onNotSuccessfulTest($e);
    }
}
