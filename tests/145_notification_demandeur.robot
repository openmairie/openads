*** Settings ***
Documentation     Notification des demandeurs

# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown

*** Keywords ***
Valider le formulaire de notification
    [Documentation]  Clique sur le bouton de validation du formulaire
    ...  de notification manuelle. Vérifie que la validation a bien été
    ...  enregistré et que la page ne contiens pas d'erreur.
    ...  Récupère et renvoie la date et l'heure de validation du formulaire.

    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Wait Until Page Contains  La notification a été générée.
    La page ne doit pas contenir d'erreur
    [Return]  ${CurrentDate}

*** Test Cases ***
Constitution du jeu de données
    [Documentation]  constitution d'un jeu de données servant à tester le bon fonctionnement
    ...  de la notification des demandeurs

    Depuis la page d'accueil  admin  admin
    # Isolation du contexte
    &{librecom_values} =  Create Dictionary
    ...  om_collectivite_libelle=LIBRECOM_NOTIFDEM
    ...  departement=020
    ...  commune=001
    ...  insee=20001
    ...  direction_code=Notif
    ...  direction_libelle=Direction de LIBRECOM_NOTIFDEM
    ...  direction_chef=Chef
    ...  division_code=Notif
    ...  division_libelle=Division Notif
    ...  division_chef=Chef
    ...  guichet_om_utilisateur_nom=Durandana Paquet
    ...  guichet_om_utilisateur_email=dpaquet@openads-test.fr
    ...  guichet_om_utilisateur_login=dpaquet
    ...  guichet_om_utilisateur_pwd=dpaquet
    ...  instr_om_utilisateur_nom=Mandel Paulet
    ...  instr_om_utilisateur_email=mpaulet@openads-test.fr
    ...  instr_om_utilisateur_login=mpaulet
    ...  instr_om_utilisateur_pwd=mpaulet
    ...  acteur=plop
    Isolation d'un contexte  ${librecom_values}

    # paramètrage du titre et du message de notificatio
    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_type_titre
    ...  valeur=[openADS] Notification concernant votre dossier [DOSSIER]
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_service_type_titre
    ...  valeur=[openADS] Notification pour les services concernant le dossier [DOSSIER]
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_tiers_type_titre
    ...  valeur=[openADS] Notification pour les tiers concernant le dossier (avec un caractère accentué) (avec un caractère accentué) [DOSSIER]
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_type_message
    ...  valeur=Bonjour, veuillez prendre connaissance du(des) document(s) suivant(s) :<br> [LIEN_TELECHARGEMENT_DOCUMENT]<br>[LIEN_TELECHARGEMENT_ANNEXE]
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}
    
    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_service_type_message
    ...  valeur=Bonjour les services, veuillez prendre connaissance du(des) document(s) suivant(s) :<br> [LIEN_TELECHARGEMENT_DOCUMENT]<br>[LIEN_TELECHARGEMENT_ANNEXE]
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=parametre_courriel_tiers_type_message
    ...  valeur=Bonjour les tiers (avec un caractère accentué), veuillez prendre connaissance du(des) document(s) suivant(s) :<br> [LIEN_TELECHARGEMENT_DOCUMENT]<br>[LIEN_TELECHARGEMENT_ANNEXE]
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=parametre_notification_url_acces
    ...  valeur=http://localhost/openads/
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    # lettretype
    &{args_lettretype} =  Create Dictionary
    ...  id=test_NOTIF
    ...  libelle=Test
    ...  sql=Aucune REQUÊTE
    ...  titre=&idx, &destinataire, aujourdhui&aujourdhui, datecourrier&datecourrier, &departement
    ...  corps=Ceci est un document
    ...  actif=true
    ...  collectivite=agglo
    Ajouter la lettre-type depuis le menu  &{args_lettretype}

    # Ajout de 9 événements pour tester tous les cas de notification
    # 4 événements avec des lettretypes sans annexes
    @{etat_source} =  Create List  delai de notification envoye
    @{type_di} =  Create List  PCI - P - Initial
    &{args_evenement1} =  Create Dictionary
    ...  libelle=TEST_NOTIF_AUTO_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification=Notification automatique
    Ajouter l'événement depuis le menu  ${args_evenement1}

    &{args_evenement2} =  Create Dictionary
    ...  libelle=TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification=Notification automatique avec signature requise
    Ajouter l'événement depuis le menu  ${args_evenement2}

    &{args_evenement3} =  Create Dictionary
    ...  libelle=TEST_NOTIF_MAN_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification=Notification manuelle
    Ajouter l'événement depuis le menu  ${args_evenement3}

    &{args_evenement4} =  Create Dictionary
    ...  libelle=TEST_NOTIF_MAN_SIGN_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification=Notification manuelle avec signature requise
    Ajouter l'événement depuis le menu  ${args_evenement4}

    # 2 événements sans lettretypes sans annexe
    &{args_evenement5} =  Create Dictionary
    ...  libelle=TEST_NOTIF_AUTO
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  notification=Notification automatique
    Ajouter l'événement depuis le menu  ${args_evenement5}

    &{args_evenement6} =  Create Dictionary
    ...  libelle=TEST_NOTIF_MAN
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  notification=Notification manuelle
    Ajouter l'événement depuis le menu  ${args_evenement6}

    # 3 événements avec annexe
    &{args_evenement7} =  Create Dictionary
    ...  libelle=TEST_NOTIF_MAN_ANNEXE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  notification=Notification manuelle avec annexe
    Ajouter l'événement depuis le menu  ${args_evenement7}

    &{args_evenement8} =  Create Dictionary
    ...  libelle=TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification=Notification manuelle avec annexe
    Ajouter l'événement depuis le menu  ${args_evenement8}

    &{args_evenement9} =  Create Dictionary
    ...  libelle=TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification=Notification manuelle avec annexe et avec signature requise
    Ajouter l'événement depuis le menu  ${args_evenement9}

    # Évenements dont les services consultés peuvent être notifiés
    &{args_evenement10} =  Create Dictionary
    ...  libelle=TEST_NOTIF_SC_SANS_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  notification_service=true
    Ajouter l'événement depuis le menu  ${args_evenement10}

    &{args_evenement11} =  Create Dictionary
    ...  libelle=TEST_NOTIF_SC_AVEC_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification_service=true
    Ajouter l'événement depuis le menu  ${args_evenement11}

    # Évenements dont les tiers consultés peuvent être notifiés
    &{args_evenement10} =  Create Dictionary
    ...  libelle=TEST_NOTIF_TC_SANS_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  notification_tiers=Notification manuelle
    Ajouter l'événement depuis le menu  ${args_evenement10}

    &{args_evenement11} =  Create Dictionary
    ...  libelle=TEST_NOTIF_TC_AVEC_LETTRETYPE
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification_tiers=Notification manuelle
    Ajouter l'événement depuis le menu  ${args_evenement11}

    # Création de deux services d'instruction notifiable et d'un non notifiable
    &{service} =  Create Dictionary
    ...  abrege=00.00
    ...  libelle=ServiceNotifiable1
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  service_type=openADS
    ...  generate_edition=true
    ...  accepte_notification_email=true
    ...  email=notifiable1@ok.fr
    Ajouter le service depuis le listing  ${service}

    &{service} =  Create Dictionary
    ...  abrege=00.01
    ...  libelle=ServiceNotifiable2
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  service_type=openADS
    ...  generate_edition=true
    ...  accepte_notification_email=true
    ...  email=notifiable2@ok.fr\nnotifiable3@ok.fr\nnotifiable4@ok.fr
    Ajouter le service depuis le listing  ${service}

    &{service} =  Create Dictionary
    ...  abrege=00.02
    ...  libelle=ServiceNonNotifiable
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  service_type=openADS
    ...  generate_edition=true
    ...  accepte_notification_email=false
    ...  email=nnotifiable@nope.fr
    Ajouter le service depuis le listing  ${service}

    ${om_collectivite_tiers} =  Create List
    ...  LIBRECOM_NOTIFDEM
    Set Suite Variable  ${om_collectivite_tiers}

    # Création d'une catégorie de tiers consulté (obligatoire pour pouvoir créer des tiers)
    ${current_date} =  Get Current Date  result_format=%d/%m/%Y
    ${nextDay} =  Add Time To Date  ${current_date}  1 days  %d/%m/%Y  True  %d/%m/%Y
    &{categorie_tiers} =  Create Dictionary
    ...  code=CAT_TEST
    ...  libelle=Categorie test notif tiers
    ...  description=Categorie servant pour le test de la notification des tiers consultés.
    ...  date_debut_validite=${current_date}
    ...  date_fin_validite=${nextDay}
    ...  om_collectivite=${om_collectivite_tiers}
    Ajouter la categorie de tiers consulte  ${categorie_tiers}

    # Création de deux tiers d'instruction notifiable et d'un non notifiable
    &{tiers} =  Create Dictionary
    ...  abrege=00.00
    ...  libelle=TiersNotifiable1
    ...  categorie_tiers_consulte=Categorie test notif tiers
    ...  accepte_notification_email=true
    ...  liste_diffusion=tnotifiable1@ok.fr
    Ajouter le tiers consulte depuis le listing  ${tiers}

    &{tiers} =  Create Dictionary
    ...  abrege=00.01
    ...  libelle=TiersNotifiable2
    ...  categorie_tiers_consulte=Categorie test notif tiers
    ...  accepte_notification_email=true
    ...  liste_diffusion=tnotifiable2@ok.fr\ntnotifiable3@ok.fr\ntnotifiable4@ok.fr
    Ajouter le tiers consulte depuis le listing  ${tiers}

    &{tiers} =  Create Dictionary
    ...  abrege=00.02
    ...  libelle=TiersNonNotifiable
    ...  categorie_tiers_consulte=Categorie test notif tiers
    ...  accepte_notification_email=false
    ...  liste_diffusion=nnotifiable@nope.fr
    Ajouter le tiers consulte depuis le listing  ${tiers}


Activation de la notification par mail
    [Documentation]  Active la notification par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    &{om_param} =  Create Dictionary
    ...  libelle=option_notification
    ...  valeur=mail
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    Démarrer maildump


Notification automatique par mail d'une instruction sans lettretype
    [Documentation]  Vérifie le bon fonctionnement de la notification automatique
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Cressac
    ...  particulier_prenom=Véronique
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=vcressac@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Charpie
    ...  particulier_prenom=Aimé
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=caime@notnotif.fr

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique sans lettretype
    # La notification doit se faire à l'ajout de l'instruction
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_AUTO

    # Vérification de l'affichage du tableau de suivi
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_AUTO
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  vcressac@notif.fr
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_AUTO
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    Element Should Not Contain  css=td[data-column-id="destinataire"]  caime@notnotif.fr
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # On vérifie qu'un dossier déjà notifié ne peut pas être supprimé
    Depuis la page d'accueil  admin  admin

    # On active l'option de suppression
    &{om_param} =  Create Dictionary
    ...  libelle=option_suppression_dossier_instruction
    ...  valeur=true
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    Depuis le contexte du dossier d'instruction  ${di_notif_auto1}

    Portlet Action Should Not Be In Form  dossier_instruction  supprimer

    # On désactive l'option de suppression
    &{om_param} =  Create Dictionary
    ...  libelle=option_suppression_dossier_instruction
    ...  valeur=false
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    # Vérification de l'envoi du mail et du contenu du mail
    Verifier que le mail a bien été envoyé au destinataire  vcressac@notif.fr
    Page Should Not Contain  caime@notnotif.fr
    # le mail ne doit pas contenir de lien car il n'y a pas de pièce
    Vérifier que le contenu du mail ne contiens pas  vcressac@notif.fr  /web/notification.php?key=

    ## Vérification de la notification automatique des tiers
    Depuis la page d'accueil  admin  admin

    @{etat_source} =  Create List  delai de notification envoye
    @{type_di} =  Create List  PCI - P - Initial
    &{args_evenement1} =  Create Dictionary
    ...  libelle=accepter un dossier sans réserve
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  notification_tiers=Notification automatique
    Ajouter l'événement depuis le menu  ${args_evenement1}
    
    # Liste des arguments pour la demande
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    # Liste des arguments pour le pétitionnaire
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Rodolphe
    ...  particulier_prenom=Desseauve
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Ajouter une instruction au DI  ${di}  ${args_evenement1.libelle}


Notification automatique par mail d'une instruction avec lettretype sans signature requise
    [Documentation]  Vérifie le bon fonctionnement de la notification automatique
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Patience
    ...  particulier_prenom=Boncoeur
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=pboncoeur@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Veronneau
    ...  particulier_prenom=Vail
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique sans la finaliser pour vérifier
    # que l'action d'envoi manuelle de la notification n'est pas visible
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_AUTO_LETTRETYPE
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_AUTO_LETTRETYPE
    Portlet Action Should Not Be In SubForm  instruction  overlay_notification_manuelle
    # Finalisation de l'instruction ce qui doit déclencher l'envoi de la notification automatique
    Click On SubForm Portlet Action  instruction  finaliser
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y

    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  pboncoeur@notif.fr
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_AUTO_LETTRETYPE
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    Element Should Not Contain  css=td[data-column-id="destinataire"]  vail
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'envoi du mail et du contenu du mail
    Verifier que le mail a bien été envoyé au destinataire  pboncoeur@notif.fr
    Page Should Not Contain  vail
    # le mail doit contenir le lien vers la pièce
    Vérifier le contenu du mail  pboncoeur@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_AUTO_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}


Notification automatique par mail d'une instruction avec lettretype et avec retour signature
    [Documentation]  Vérifie le bon fonctionnement de la notification automatique
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=Denomination
    ...  personne_morale_nom=Monjeau
    ...  personne_morale_prenom=Eglantine
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=meglantine@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_raison_sociale=raison sociale
    ...  personne_morale_nom=Bonenfant
    ...  personne_morale_prenom=Anne
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=abonenfant@notnotif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique avec lettretype avec signature
    # La notification doit se faire à l'ajout de la date de retour
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Portlet Action Should Not Be In SubForm  instruction  overlay_notification_manuelle
    Page Should Not Contain Element  css=fieldset#fieldset-sousform-instruction-suivi-notification
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Click On Submit Button In Subform

    # Vérification de l'affichage du tableau de suivi
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="émetteur"]      admin (Administrateur)
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=div#suivi_notification_jsontotab  meglantine@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  abonenfant@notnotif.fr
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="instruction"]  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="statut"]       envoyé
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'envoi des mails et de leur contenu
    Verifier que le mail a bien été envoyé au destinataire  meglantine@notif.fr
    Vérifier le contenu du mail  meglantine@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document

    Verifier que le mail a bien été envoyé au destinataire  abonenfant@notnotif.fr
    Vérifier le contenu du mail  abonenfant@notnotif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}


Notification manuelle par mail d'une instruction sans lettretype
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Loiselle
    ...  particulier_prenom=Roland
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=rloiselle@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Dandonneau
    ...  particulier_prenom=Parfait
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=dparfait@notnotif.fr

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique sans lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_man} =  Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_MAN
    Click On Link  ${inst_notif_man}
    # L'action ne doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    Wait until element contains  css=div#sousform-instruction_notification_manuelle  Loiselle Roland | rloiselle@notif.fr | pétitionnaire principal
    Element Should Not Contain  css=div#sousform-instruction_notification_manuelle  dparfait@notnotif.fr
    Page Should Not Contain Element  css=select#annexes_documents
    # Sélection du demandeur et validation
    Select Checkbox  css=div#sousform-instruction_notification_manuelle input[type="checkbox"]
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  dparfait@notnotif.fr
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  rloiselle@notif.fr
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé

    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  rloiselle@notif.fr
    Page Should Not Contain  dparfait@notnotif.fr
    # le mail ne doit pas contenir de lien car il n'y a pas de pièce
    Vérifier que le contenu du mail ne contiens pas  rloiselle@notif.fr  /web/notification.php?key=


Notification manuelle par mail d'une instruction avec lettretype sans signature requise
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=denom1
    ...  personne_morale_nom=Leclerc
    ...  personne_morale_prenom=Maurelle
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=denom2
    ...  personne_morale_nom=Jalbert
    ...  personne_morale_prenom=Matthieu
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=mjalbert@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification manuelle avec lettretype sans signature
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE
    # L'action doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    Wait until element contains  css=div#sousform-instruction_notification_manuelle  denom2 représenté par Jalbert Matthieu | mjalbert@notif.fr | pétitionnaire
    Element Should Not Contain  css=div#sousform-instruction_notification_manuelle  Leclerc
    Page Should Not Contain Element  css=select#annexes_documents
    # Sélection du demandeur et validation
    Select Checkbox  css=div#sousform-instruction_notification_manuelle input[type="checkbox"]
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  leclerc
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  mjalbert@notif.fr
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé

    # Vérifie que le mail a bien été envoyé
    Verifier que le mail a bien été envoyé au destinataire  mjalbert@notif.fr
    Page Should Not Contain  leclerc
    # le mail doit contenir le lien vers la pièce
    Vérifier le contenu du mail  mjalbert@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}


Notification manuelle par mail d'une instruction avec lettretype et signature requise
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Babin
    ...  particulier_prenom=Pauline
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=pbabin@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Chenard
    ...  particulier_prenom=Lance
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=lchenard@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique avec lettretype avec signature
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE
    # L'action ne doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE
    Portlet Action Should Not Be In SubForm  instruction  overlay_notification_manuelle
    # Ajout d'une date de signature
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    Wait until element contains  css=div#sousform-instruction_notification_manuelle  Chenard Lance | lchenard@notif.fr | pétitionnaire
    Element Should Contain  css=div#sousform-instruction_notification_manuelle  Babin Pauline | pbabin@notif.fr | pétitionnaire principal
    Page Should Not Contain Element  css=select#annexes_documents
    # Sélection du demandeur et validation
    Select Checkbox  css=div#sousform-instruction_notification_manuelle div.bloc:nth-child(1) > div:nth-child(2) input
    Select Checkbox  css=div#sousform-instruction_notification_manuelle div.bloc:nth-child(1) > div:nth-child(3) input
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="émetteur"]      admin (Administrateur)
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=div#suivi_notification_jsontotab  lchenard@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  pbabin@notif.fr
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="instruction"]  TEST_NOTIF_MAN_SIGN_LETTRETYPE
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="statut"]       envoyé
    Element Should Contain  css=tbody tr:nth-child(2) td[data-column-id="commentaire"]  Le mail de notification a été envoyé

    # Vérifie que les mails ont bien été envoyés et qu'ils contiennent le lien vers la pièce
    Verifier que le mail a bien été envoyé au destinataire  lchenard@notif.fr
    Vérifier le contenu du mail  lchenard@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document

    Verifier que le mail a bien été envoyé au destinataire  pbabin@notif.fr
    Vérifier le contenu du mail  pbabin@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}


Notification manuelle par mail d'une instruction sans lettretype avec annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin
    # Ajout d'un dossier et d'une instruction de notification manuelle sans lettretype
    # avec une annexe
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Gadbois
    ...  particulier_prenom=Agnès
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=agadbois@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Houle
    ...  particulier_prenom=Fanchon
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=hfanchon@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # finalisation et ajout d'une date de retour signature sur une instruction
    # pour pouvoir la choisir comme annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de l'instruction de notification
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_man_annexe} =  Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_MAN_ANNEXE
    Click On Link  ${inst_notif_man_annexe}
    # L'action ne doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'affichage du formulaire de notif manuelle :
    # les 2 pétitionnaires et le champs de sélection de l'annexe doivent être visible
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    Wait until element contains  css=div#sousform-instruction_notification_manuelle  Gadbois Agnès | agadbois@notif.fr | pétitionnaire principal
    Element Should Contain  css=div#sousform-instruction_notification_manuelle  Houle Fanchon | hfanchon@notif.fr | pétitionnaire 

    # Sélection d'un demandeur et validation
    Select Checkbox  xpath=//label[normalize-space(text()) = 'Houle Fanchon | hfanchon@notif.fr | pétitionnaire']//ancestor::div[contains(@class, 'field-type-checkbox')]//input[contains(@type, 'checkbox')]
    @{liste_documents}  Create List  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Not Contain  css=div#suivi_notification_jsontotab  agadbois@notif.fr
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  hfanchon@notif.fr
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN_ANNEXE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé

    # Vérifie que le mail a bien été envoyé et qu'il contiens un lien vers l'annexe
    Verifier que le mail a bien été envoyé au destinataire  hfanchon@notif.fr
    Vérifier le contenu du mail  hfanchon@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  RECEPISSE DE DEPOT
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_ANNEXE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}


Notification manuelle par mail d'une instruction avec lettretype avec annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin
    # Ajout d'un dossier et d'une instruction de notification manuelle sans lettretype
    # avec une annexe
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Létourneau
    ...  particulier_prenom=Jules
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=jletourneau@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Charpentier
    ...  particulier_prenom=Medoro
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=mcharpentier@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # finalisation et ajout d'une date de retour signature sur une instruction du di
    # pour pouvoir la choisir comme annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de l'instruction et finalisation
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    # L'action doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'affichage du formulaire de notif manuelle :
    # les 2 pétitionnaires et le champs de sélection de l'annexe doivent être visible
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    Wait until element contains  css=div#sousform-instruction_notification_manuelle  Létourneau Jules | jletourneau@notif.fr | pétitionnaire principal
    Element Should Contain  css=div#sousform-instruction_notification_manuelle  Charpentier Medoro | mcharpentier@notif.fr | pétitionnaire 
    
    # Sélection d'un demandeur et validation
    Select Checkbox  xpath=//label[normalize-space(text()) = 'Charpentier Medoro | mcharpentier@notif.fr | pétitionnaire']//ancestor::div[contains(@class, 'field-type-checkbox')]//input[contains(@type, 'checkbox')]
    @{liste_documents}  Create List  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  jletourneau@notif.fr
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  mcharpentier@notif.fr
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé

    # Vérifie que le mail a bien été envoyé et qu'il contiens les liens vers la pièce et l'annexe
    Verifier que le mail a bien été envoyé au destinataire  mcharpentier@notif.fr
    Vérifier le contenu du mail  mcharpentier@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    ${key}=  Get From List  ${keys}  1
    Verifier que le lien de notification contiens  ${key}  RECEPISSE DE DEPOT

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}


Notification manuelle par mail d'une instruction avec lettretype, signature requise et annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin
    # Ajout d'un dossier et d'une instruction de notification manuelle sans lettretype
    # avec une annexe
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=DeGrasse
    ...  particulier_prenom=Charlot
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=dcharlot@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Jetté
    ...  particulier_prenom=Edmee
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=jedmee@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # finalisation et ajout d'une date de retour signature sur une instruction du di
    # pour pouvoir la choisir comme annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de l'instruction et finalisation
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    # L'action ne doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    Portlet Action Should Not Be In SubForm  instruction  overlay_notification_manuelle
    # Ajout d'une date de signature
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'affichage du formulaire de notif manuelle :
    # les 2 pétitionnaires et le champs de sélection de l'annexe doivent être visible
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    Wait until element contains  css=div#sousform-instruction_notification_manuelle  DeGrasse Charlot | dcharlot@notif.fr | pétitionnaire principal
    Element Should Contain  css=div#sousform-instruction_notification_manuelle  Jetté Edmee | jedmee@notif.fr | pétitionnaire
    
    # Sélection d'un demandeur et validation
    Select Checkbox  xpath=//label[normalize-space(text()) = 'Jetté Edmee | jedmee@notif.fr | pétitionnaire']//ancestor::div[contains(@class, 'field-type-checkbox')]//input[contains(@type, 'checkbox')]
    @{liste_documents}  Create List  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      admin (Administrateur)
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  jedmee@notif.fr
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé

    # Vérifie que le mail a bien été envoyé et qu'il contiens les liens vers la pièce et l'annexe
    Verifier que le mail a bien été envoyé au destinataire  jedmee@notif.fr
    Vérifier le contenu du mail  jedmee@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${CurrentDate}=  Get Current Date  result_format=%d/%m/%Y
    ${key}=  Get From List  ${keys}  1
    Verifier que le lien de notification contiens  ${key}  RECEPISSE DE DEPOT

    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}


Notification avec annexes multiples par mail
    [Documentation]  Vérifie à l'ouverture du formulaire de notification que le
    ...  message d'information indique bien le nombre maximum d'annexes acceptées.
    ...  Vérifie que si l'utilisateur sélectionne plus de 5 annexes un message
    ...  d'erreur s'affiche à la validation du formulaire et le formulaire est
    ...  re-affiché.
    ...  Vérifie également que toutes les annexes sont bien transmises dans le mail
    ...  de notification.

    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=DeGrasse
    ...  particulier_prenom=Charlot
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=dcharlot@notif.fr
    ...  notification=t
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_limit} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # finalisation et ajout d'une date de retour signature sur une instruction du di
    # pour pouvoir la choisir comme annexe
    Depuis la page d'accueil  admin  admin
    Depuis l'instruction du dossier d'instruction  ${di_notif_limit}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de 4 pièces qui pourront être sélectionnées comme annexe
    @{liste_pieces}  Create List  autres pièces composant le dossier (A0)  arrêté retour préfecture  certificat conformité totale lotissement  avis obligatoires
    @{title_piece_annexes}  Create List
    : FOR  ${piece}  IN  @{liste_pieces}
    \  &{document_numerise_values} =  Create Dictionary
    \  ...  uid_upload=testImportManuel.pdf
    \  ...  date_creation=10/09/2016
    \  ...  document_numerise_type=${piece}
    \  Ajouter une pièce depuis le dossier d'instruction  ${di_notif_limit}  ${document_numerise_values}
    # Récupère le nom du fichier et l'associe à celui de la pièce pour obtenir le titre de l'annexe
    \  Click On Back Button In SubForm
    \  ${nom_fichier} =  Get Text  xpath=//a[normalize-space(text()) = "${piece}"]//ancestor::tr/td[contains(@class, "firstcol")]/a/span[contains(@title, "Télécharger")]
    \  ${title_annexe} =  Catenate  ${nom_fichier}  -  ${piece}
    \  Append To List  ${title_piece_annexes}  ${title_annexe}
    # Supprime le dernier titre car la dernière pièce ne sera pas transmise lors de la notification
    Remove From List  ${title_piece_annexes}  3

    # Ajout d'une consultation et rendu d'avis pour pouvoir la choisir comme annexe
    Ajouter une consultation depuis un dossier  ${di_notif_limit}  00.02 - ServiceNonNotifiable
    Depuis le contexte de la consultation  ${di_notif_limit}  00.02 - ServiceNonNotifiable
    &{piece_values} =  Create Dictionary
    ...  fichier_upload=testImportManuel2.pdf
    ...  date_demande=03/02/2016
    ...  avis_consultation=Tacite
    ${nom_piece} =  Ajouter une pièce à la consultation  ${piece_values}

    # Connexion en tant qu'instructeur du dossier
    # Ajout d'une instruction notifiable à laquelle on peut ajouter des annexes
    Ajouter une instruction au DI et la finaliser  ${di_notif_limit}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE

    # Accès au formulaire de notification manuelle et vérification du message d'info
    Depuis la page d'accueil  mpaulet  mpaulet
    Depuis l'instruction du dossier d'instruction  ${di_notif_limit}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale

    # Sélection du demandeur
    Select Checkbox  css=input[type="checkbox"]
    # Sélection de toutes les annexes possibles
    Select From Multiple Chosen List  annexes_pieces  ${liste_pieces}
    @{liste_documents}  Create List  Avis - ServiceNonNotifiable  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}

    # Validation du formulaire et vérification du message d'erreur
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    Wait until keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL} 
    ...  Error Message Should Contain  Plus de 5 annexes ont été sélectionnées vous devez en supprimer 1 pour que les pétitionnaires soient notifiés.

    # Déselection de certaines pièces pour n'en garder que 5
    @{liste_pieces_unselect}  Create List  avis obligatoires
    Unselect From Multiple Chosen List  annexes_pieces  ${liste_pieces_unselect}
    Click Element Until New Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]  css=.message.ui-state-valid
    Wait Until Element Contains 
    ...  css=.message.ui-state-valid 
    ...  La notification a été générée.\nLes pièces et documents suivants seront envoyés :\nTEST_NOTIF_MAN_LETTRETYPE_ANNEXE\ncertificat conformité totale lotissement\nautres pièces composant le dossier (A0)\narrêté retour préfecture\nAvis - ServiceNonNotifiable\nNotification du delai legal maison individuelle

    # Affichage de la liste des annexes dans le tableau de suivi
    Click Link  css=.ui-dialog-titlebar-close
    Wait Until Page Contains Element  css=td[data-column-id="annexes"]
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe\nAnnexe\nAnnexe\nAnnexe\nAnnexe

    # Affichage du nom de l'élement dans le tooltip. Pour ça on vérifie que l'élément contiens bien
    # un attribut title ayant le nom de la pièce
    # Récupération des attributs des annexes et stockage dans une liste
    # On vérifie également l'affichage de la page de téléchargement du document
    @{contenu_annexes}  Create List    TEST IMPORT MANUEL 1  TEST IMPORT MANUEL 1  TEST IMPORT MANUEL 1  TEST IMPORT MANUEL 2  RECEPISSE DE DEPOT
    @{liste_titre_annexes}  Create List
    : FOR  ${index}  IN RANGE  1  6
    \  ${tooltip} =  Get Element Attribute  css=td[data-column-id="annexes"] li:nth-child(${index}) a  title
    \  Append To List  ${liste_titre_annexes}  ${tooltip}
    \  Click Link  css=td[data-column-id="annexes"] li:nth-child(${index}) a
    # Récupération du contenu du document et vérification du contenu dans la page
    # de téléchargement
    \  ${index_contenu} =  Evaluate  ${index} - 1
    \  ${contenu} =  Get From List  ${contenu_annexes}  ${index_contenu}
    \  Select Window  NEW
    \  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${contenu}
    # Ferme la fenêtre de récupération du document et retourne sur l'application
    \  Close Window
    \  Select Window
    # Vérifie que les titres des documents existe bien dans la liste
    List Should Contain Sub List  ${liste_titre_annexes}  ${liste_documents}
    # Vérifie que les titres des pièces existent bien dans la liste
    List Should Contain Sub List  ${liste_titre_annexes}  ${title_piece_annexes}

    # Test de l'accès aux différents types de pièce et document
    Verifier que le mail a bien été envoyé au destinataire  dcharlot@notif.fr
    Vérifier le contenu du mail  dcharlot@notif.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${key}=  Get From List  ${keys}  1
    Verifier que le lien de notification contiens  ${key}  TEST IMPORT MANUEL 1
    ${key}=  Get From List  ${keys}  2
    Verifier que le lien de notification contiens  ${key}  TEST IMPORT MANUEL 1
    ${key}=  Get From List  ${keys}  3
    Verifier que le lien de notification contiens  ${key}  TEST IMPORT MANUEL 1
    ${key}=  Get From List  ${keys}  4
    Verifier que le lien de notification contiens  ${key}  TEST IMPORT MANUEL 2
    ${key}=  Get From List  ${keys}  5
    Verifier que le lien de notification contiens  ${key}  RECEPISSE DE DEPOT


Notification par mail des services consultés instruction sans lettretype et sans annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Carrière
    ...  particulier_prenom=Élisabeth
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_sc_ss_lt1} =  Ajouter une instruction au DI  ${di_notif_SC1}  TEST_NOTIF_SC_SANS_LETTRETYPE
    Click On Link  ${inst_notif_sc_ss_lt1}
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_service_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_service_consulte  modale
    Wait until page Contains  ServiceNotifiable1
    Page Should Contain  ServiceNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete #form-content:nth-child(1)   ServiceNonNotifiable
    # Remplissage du formulaire et validation
    Select Checkbox  css=div#sousform-instruction_notification_manuelle div.bloc:nth-child(1) > div:nth-child(2) input
    Select Checkbox  css=div#sousform-instruction_notification_manuelle div.bloc:nth-child(1) > div:nth-child(3) input
    ${CurrentDate} =  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-service  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  ServiceNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_SC_SANS_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  notifiable1@ok.fr
    Verifier que le mail a bien été envoyé au destinataire  notifiable2@ok.fr
    Verifier que le mail a bien été envoyé au destinataire  notifiable3@ok.fr
    Verifier que le mail a bien été envoyé au destinataire  notifiable4@ok.fr
    # le mail ne doit pas contenir de lien car il n'y a pas de pièce
    Vérifier que le contenu du mail ne contiens pas  notifiable1@ok.fr  /web/notification.php?key=
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les services, veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les services concernant le dossier
    Vider la boite mail


Notification par mail des services consultés instruction avec lettretype et sans annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Soucy
    ...  particulier_prenom=Galatee
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC2} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_SC2}  TEST_NOTIF_SC_AVEC_LETTRETYPE
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_service_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_service_consulte  modale
    Wait until page Contains  ServiceNotifiable1
    Page Should Contain  ServiceNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete #form-content:nth-child(1)  ServiceNonNotifiable
    # Remplissage du formulaire et validation
    Select Checkbox  css=div#sousform-instruction_notification_manuelle div.bloc:nth-child(1) > div:nth-child(2) input
    ${CurrentDate}=  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-service  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  ServiceNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_SC_AVEC_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  notifiable1@ok.fr
    Page Should Not Contain  notifiable2@ok.fr
    # Le mail doit contenir le lien vers le document de l'instruction
    Vérifier le contenu du mail  notifiable1@ok.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les services, veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les services concernant le dossier
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC2}  TEST_NOTIF_SC_AVEC_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}
    Vider la boite mail


Notification par mail des services consultés instruction sans lettretype et avec annexes
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Desnoyers
    ...  particulier_prenom=Ogier
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC3} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction finalisée et signé qui servira d'annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC3}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout d'un retour d'avis
    Ajouter une consultation depuis un dossier  ${di_notif_SC3}  00.02 - ServiceNonNotifiable
    Depuis le contexte de la consultation  ${di_notif_SC3}  00.02 - ServiceNonNotifiable
    &{piece_values} =  Create Dictionary
    ...  fichier_upload=testImportManuel.pdf
    ...  date_demande=03/02/2016
    ...  avis_consultation=Tacite
    ${nom_piece} =  Ajouter une pièce à la consultation  ${piece_values}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_sc_ss_lt3} =  Ajouter une instruction au DI  ${di_notif_SC3}  TEST_NOTIF_SC_SANS_LETTRETYPE
    Click On Link  ${inst_notif_sc_ss_lt3}
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_service_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_service_consulte  modale
    Wait until page Contains  ServiceNotifiable1
    Page Should Contain  ServiceNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete .bloc:nth-child(1)  ServiceNonNotifiable
    # Remplissage du formulaire et validation
    Select Checkbox  css=div#sousform-instruction_notification_manuelle div.bloc:nth-child(1) > div:nth-child(2) input
    @{annexes_a_selectionner} =  Create List  Avis - ServiceNonNotifiable  Notification du delai legal maison individuelle
    Select From Multiple Chosen List   annexes  ${annexes_a_selectionner}
    ${CurrentDate}=  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-service  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  ServiceNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_SC_SANS_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe\nAnnexe
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  notifiable1@ok.fr
    Page Should Not Contain  notifiable2@ok.fr
    # Le mail doit contenir le lien vers les annexes
    Vérifier le contenu du mail  notifiable1@ok.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les services, veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les services concernant le dossier
    ${annexe1}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${annexe1}  RECEPISSE DE DEPOT
    ${annexe2}=  Get From List  ${keys}  1
    Verifier que le lien de notification contiens  ${annexe2}  TEST IMPORT MANUEL 1

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC3}  TEST_NOTIF_SC_SANS_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}
    Vider la boite mail


Notification par mail des services consultés instruction avec lettretype et avec annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Lanoie
    ...  particulier_prenom=Hortense
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC4} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction finalisée et signé qui servira d'annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC4}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout d'un retour d'avis
    Ajouter une consultation depuis un dossier  ${di_notif_SC4}  00.02 - ServiceNonNotifiable
    Depuis le contexte de la consultation  ${di_notif_SC4}  00.02 - ServiceNonNotifiable
    &{piece_values} =  Create Dictionary
    ...  fichier_upload=testImportManuel.pdf
    ...  date_demande=03/02/2016
    ...  avis_consultation=Tacite
    ${nom_piece} =  Ajouter une pièce à la consultation  ${piece_values}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_SC4}  TEST_NOTIF_SC_AVEC_LETTRETYPE
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_service_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_service_consulte  modale
    Wait until page Contains  ServiceNotifiable1
    Page Should Contain  ServiceNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete .bloc:nth-child(1)  ServiceNonNotifiable
    # Remplissage du formulaire et validation
    Select Checkbox  css=div#sousform-instruction_notification_manuelle div.bloc:nth-child(1) > div:nth-child(2) input
    @{annexes_a_selectionner} =  Create List  Avis - ServiceNonNotifiable  Notification du delai legal maison individuelle
    Select From Multiple Chosen List   annexes  ${annexes_a_selectionner}
    ${CurrentDate}=  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-service  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  ServiceNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_SC_AVEC_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe\nAnnexe
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  notifiable1@ok.fr
    Page Should Not Contain  notifiable2@ok.fr
    # Le mail doit contenir le lien vers le document et les annexes
    Vérifier le contenu du mail  notifiable1@ok.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les services, veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les services concernant le dossier
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${annexe1}=  Get From List  ${keys}  1
    Verifier que le lien de notification contiens  ${annexe1}  RECEPISSE DE DEPOT
    ${annexe2}=  Get From List  ${keys}  2
    Verifier que le lien de notification contiens  ${annexe2}  TEST IMPORT MANUEL 1

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC4}  TEST_NOTIF_SC_AVEC_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}
    Vider la boite mail


Notification par mail des tiers consultés instruction sans lettretype et sans annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Carrière
    ...  particulier_prenom=Élisabeth
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_tc_ss_lt1} =  Ajouter une instruction au DI  ${di_notif_SC1}  TEST_NOTIF_TC_SANS_LETTRETYPE
    Click On Link  ${inst_notif_tc_ss_lt1}
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_tiers_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_tiers_consulte  modale
    Wait until page Contains  TiersNotifiable1
    Page Should Contain  TiersNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete #form-content:nth-child(1)   TiersNonNotifiable
    # Remplissage du formulaire et validation
    @{tiers_a_selectionner} =  Create List  TiersNotifiable1  TiersNotifiable2
    Select From Multiple Chosen List  tiers_consulte  ${tiers_a_selectionner}
    ${CurrentDate} =  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-tiers  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  TiersNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_TC_SANS_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  tnotifiable1@ok.fr
    Verifier que le mail a bien été envoyé au destinataire  tnotifiable2@ok.fr
    Verifier que le mail a bien été envoyé au destinataire  tnotifiable3@ok.fr
    Verifier que le mail a bien été envoyé au destinataire  tnotifiable4@ok.fr
    # le mail ne doit pas contenir de lien car il n'y a pas de pièce
    Vérifier que le contenu du mail ne contiens pas  tnotifiable1@ok.fr  /web/notification.php?key=
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les tiers (avec un caractère accentué), veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les tiers concernant le dossier (avec un caractère accentué)
    Vider la boite mail


Notification par mail des tiers consultés instruction avec lettretype et sans annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Soucy
    ...  particulier_prenom=Galatee
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC2} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_SC2}  TEST_NOTIF_TC_AVEC_LETTRETYPE
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_tiers_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_tiers_consulte  modale
    Wait until page Contains  TiersNotifiable1
    Page Should Contain  TiersNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete #form-content:nth-child(1)  TiersNonNotifiable
    # Remplissage du formulaire et validation
    @{tiers_a_selectionner} =  Create List  TiersNotifiable1
    Select From Multiple Chosen List  tiers_consulte  ${tiers_a_selectionner}
    ${CurrentDate} =  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-tiers  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  TiersNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_TC_AVEC_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  tnotifiable1@ok.fr
    Page Should Not Contain  tnotifiable2@ok.fr
    # Le mail doit contenir le lien vers le document de l'instruction
    Vérifier le contenu du mail  tnotifiable1@ok.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les tiers (avec un caractère accentué), veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les tiers concernant le dossier (avec un caractère accentué)
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC2}  TEST_NOTIF_TC_AVEC_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}
    Vider la boite mail


Notification par mail des tiers consultés instruction sans lettretype et avec annexes
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Desnoyers
    ...  particulier_prenom=Ogier
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC3} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction finalisée et signé qui servira d'annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC3}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout d'un retour d'avis
    Ajouter une consultation depuis un dossier  ${di_notif_SC3}  00.02 - ServiceNonNotifiable
    Depuis le contexte de la consultation  ${di_notif_SC3}  00.02 - ServiceNonNotifiable
    &{piece_values} =  Create Dictionary
    ...  fichier_upload=testImportManuel.pdf
    ...  date_demande=03/02/2016
    ...  avis_consultation=Tacite
    ${nom_piece} =  Ajouter une pièce à la consultation  ${piece_values}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_tc_ss_lt3} =  Ajouter une instruction au DI  ${di_notif_SC3}  TEST_NOTIF_TC_SANS_LETTRETYPE
    Click On Link  ${inst_notif_tc_ss_lt3}
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_tiers_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_tiers_consulte  modale
    Wait until page Contains  TiersNotifiable1
    Page Should Contain  TiersNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete .bloc:nth-child(1)  TiersNonNotifiable
    # Remplissage du formulaire et validation
    @{tiers_a_selectionner} =  Create List  TiersNotifiable1
    Select From Multiple Chosen List  tiers_consulte  ${tiers_a_selectionner}
    @{annexes_a_selectionner} =  Create List  Avis - ServiceNonNotifiable  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes  ${annexes_a_selectionner}
    ${CurrentDate}=  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-tiers  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  TiersNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_TC_SANS_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe\nAnnexe
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  tnotifiable1@ok.fr
    Page Should Not Contain  tnotifiable2@ok.fr
    # Le mail doit contenir le lien vers les annexes
    Vérifier le contenu du mail  tnotifiable1@ok.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les tiers (avec un caractère accentué), veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les tiers concernant le dossier (avec un caractère accentué)
    ${annexe1}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${annexe1}  RECEPISSE DE DEPOT
    ${annexe2}=  Get From List  ${keys}  1
    Verifier que le lien de notification contiens  ${annexe2}  TEST IMPORT MANUEL 1

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC3}  TEST_NOTIF_TC_SANS_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}
    Vider la boite mail


Notification par mail des tiers consultés instruction avec lettretype et avec annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction dont les services peuvent être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Lanoie
    ...  particulier_prenom=Hortense
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_SC4} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction finalisée et signé qui servira d'annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC4}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout d'un retour d'avis
    Ajouter une consultation depuis un dossier  ${di_notif_SC4}  00.02 - ServiceNonNotifiable
    Depuis le contexte de la consultation  ${di_notif_SC4}  00.02 - ServiceNonNotifiable
    &{piece_values} =  Create Dictionary
    ...  fichier_upload=testImportManuel.pdf
    ...  date_demande=03/02/2016
    ...  avis_consultation=Tacite
    ${nom_piece} =  Ajouter une pièce à la consultation  ${piece_values}

    # Ajout de l'instruction a notifier au service qui n'a pas de lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_SC4}  TEST_NOTIF_TC_AVEC_LETTRETYPE
    # L'action doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_tiers_consulte

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  overlay_notification_tiers_consulte  modale
    Wait until page Contains  TiersNotifiable1
    Page Should Contain  TiersNotifiable2
    Element Should Not Contain  css=#sousform-instruction_notification_manuelle .formEntete .bloc:nth-child(1)  TiersNonNotifiable
    # Remplissage du formulaire et validation
    @{tiers_a_selectionner} =  Create List  TiersNotifiable1
    Select From Multiple Chosen List  tiers_consulte  ${tiers_a_selectionner}
    @{annexes_a_selectionner} =  Create List  Avis - ServiceNonNotifiable  Notification du delai legal maison individuelle
    Select From Multiple Chosen List   annexes  ${annexes_a_selectionner}
    ${CurrentDate}=  Valider le formulaire de notification

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification-tiers  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet
    Element Should Contain  css=td[data-column-id="dateD'envoi"]   ${CurrentDate}
    Element Should Contain  css=td[data-column-id="destinataire"]  TiersNotifiable1
    Element Text Should Be  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_TC_AVEC_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe\nAnnexe
    Element Should Contain  css=td[data-column-id="statut"]       envoyé
    Element Should Contain  css=td[data-column-id="commentaire"]  Le mail de notification a été envoyé
    
    # Vérification de l'envoi du mail et de son contenu
    Verifier que le mail a bien été envoyé au destinataire  tnotifiable1@ok.fr
    Page Should Not Contain  tnotifiable2@ok.fr
    # Le mail doit contenir le lien vers le document et les annexes
    Vérifier le contenu du mail  tnotifiable1@ok.fr  /web/notification.php?key=
    ${keys} =  Recuperer les cles dans le mail de notification
    # Vérifie que le message affiché est bien celui paramétré
    Page Should Contain  Bonjour les tiers (avec un caractère accentué), veuillez prendre connaissance du(des) document(s) suivant(s)
    Unselect frame
    Vérifier le sujet du mail  notifiable1@ok.fr  [openADS] Notification pour les tiers concernant le dossier (avec un caractère accentué)
    ${key}=  Get From List  ${keys}  0
    Verifier que le lien de notification contiens  ${key}  Ceci est un document
    ${annexe1}=  Get From List  ${keys}  1
    Verifier que le lien de notification contiens  ${annexe1}  RECEPISSE DE DEPOT
    ${annexe2}=  Get From List  ${keys}  2
    Verifier que le lien de notification contiens  ${annexe2}  TEST IMPORT MANUEL 1

    # Suivi de la date de 1er accès
    Depuis l'instruction du dossier d'instruction  ${di_notif_SC4}  TEST_NOTIF_TC_AVEC_LETTRETYPE
    Element Should Contain  css=td[data-column-id="dateDePremierAccès"]  ${CurrentDate}
    Vider la boite mail

Notification automatique des tiers consultés
    [Documentation]  Test de la notification automatique des tiers consultés.
    ...  Vérifie que seul les tiers ayant le type d'habilitation sélectionné
    ...  sont notifiés. Vérifie également que le service consulté en charge du
    ...  dossier n'est pas notifié et qu'uniquement les tiers associés à la
    ...  commune et au département du dossier le sont.

    # Connection en tant qu'instructeur polyvalent de la commune du dossier
    Depuis la page d'accueil  admin  admin
    # Enregistrement de la date pour la saisie des formulaire
    ${date_courante} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    # Activation de l'option option_dossier_commune et de l'option option_mode_service_consulte
    &{om_param} =  Create Dictionary
    ...  libelle=option_dossier_commune
    ...  valeur=true
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=option_mode_service_consulte
    ...  valeur=true
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}
    # ajoute le paramètre 'acteur' à la collectivité/au service
    &{om_param} =  Create Dictionary
    ...  libelle=platau_acteur_service_consulte
    ...  valeur=plop
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}
    # définir les paramètres de type de demande
    &{platau_type_demande_initial} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_PCI
    ...  valeur=DI
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${platau_type_demande_initial}
   
    # On ajoute 2 communes
    #  - 1 associée au dossier
    #  - 1 non associé au dossier
    ${lib_commune_ok} =  Set Variable  Notif Auto Tiers Consulte
    ${code_commune_ok} =  Set Variable  20001
    &{validcom1_values} =  Create Dictionary
    ...  typecom=COM
    ...  com=${code_commune_ok}
    ...  reg=20
    ...  dep=20
    ...  arr=100
    ...  tncc=0
    ...  ncc=NOTIFATC
    ...  nccenr=NOTIFATC
    ...  libelle=${lib_commune_ok}
    ...  can=20
    ...  om_validite_debut=23/11/2020
    Ajouter commune avec dates validité  ${validcom1_values}
    ${lib_commune_fail} =  Set Variable  Notif Fail Tiers Consulte
    ${code_commune_fail} =  Set Variable  20002
    &{validcom1_values} =  Create Dictionary
    ...  typecom=COM
    ...  com=${code_commune_fail}
    ...  reg=20
    ...  dep=20
    ...  arr=200
    ...  tncc=0
    ...  ncc=NOTIFFTC
    ...  nccenr=NOTIFFTC
    ...  libelle=${lib_commune_fail}
    ...  can=20
    ...  om_validite_debut=${date_courante}
    Ajouter commune avec dates validité  ${validcom1_values}

    # On ajoute 2 départements
    #  - 1 associé à la commune du dossier
    #  - 1 non associé au dossier
    ${lib_dept_ok} =  Set Variable  Dept NATC
    ${code_dept_ok} =  Set Variable  20
    &{dept_values} =  Create Dictionary
    ...  dep=${code_dept_ok}
    ...  reg=20
    ...  cheflieu=20001
    ...  tncc=0
    ...  ncc=DEPT1
    ...  nccenr=DNOTIFATC
    ...  libelle=${lib_dept_ok}
    ...  om_validite_debut=${date_courante}
    Ajouter département  ${dept_values}
    ${lib_dept_fail} =  Set Variable  Dept NFTC
    ${code_dept_fail} =  Set Variable  11
    &{dept_values} =  Create Dictionary
    ...  dep=${code_dept_fail}
    ...  reg=11
    ...  cheflieu=11000
    ...  tncc=1
    ...  ncc=DEPT2
    ...  nccenr=DNOTIFFTC
    ...  libelle=${lib_dept_fail}
    ...  om_validite_debut=${date_courante}
    Ajouter département  ${dept_values}

    # On ajoute une catégorie de tiers consulté
    ${cat_tiers} =  Set Variable  Categorie test notif auto tiers
    &{categorie_tiers} =  Create Dictionary
    ...  code=CAT_TEST_NATC
    ...  libelle=${cat_tiers}
    ...  description=Categorie servant pour le test de la notification automatique des tiers consultés.
    ...  om_collectivite=${om_collectivite_tiers}
    Ajouter la categorie de tiers consulte  ${categorie_tiers}

    # On ajoute 5 tiers consulté :
    # - 1 ayant le même uid platau que la consultation entrante
    # - 1 associé à la commune du dossier
    # - 1 associé à la mauvaise commune mais au bon département
    # - 1 qui est associé à la mauvaise commune et au mauvais département
    # - 1 qui sera associé à l'autre type d'habilitation
    ${tc_consultation_entrante} =  Set Variable  Tiers consulté du dossier (NATC)
    &{tc_values} =  Create Dictionary
    ...  categorie_tiers_consulte=${cat_tiers}
    ...  abrege=TCDNATC
    ...  libelle=${tc_consultation_entrante}
    ...  liste_diffusion=failServiceConsultant@atreal.fr
    ...  accepte_notification_email=true
    ...  uid_platau_acteur=plop
    Ajouter le tiers consulte depuis le listing  ${tc_values}
    ${tc_commune} =  Set Variable  Tiers consulté commune (NATC)
    &{tc_values} =  Create Dictionary
    ...  categorie_tiers_consulte=${cat_tiers}
    ...  abrege=TCCNATC
    ...  liste_diffusion=communeok@atreal.fr\nfailVerifMail
    ...  libelle=${tc_commune}
    ...  ville=MARSEILLE
    ...  accepte_notification_email=true
    ...  uid_platau_acteur=plop2
    Ajouter le tiers consulte depuis le listing  ${tc_values}
    ${tc_departement} =  Set Variable  Tiers consulté departement (NATC)
    &{tc_values} =  Create Dictionary
    ...  categorie_tiers_consulte=${cat_tiers}
    ...  abrege=TCDNATC
    ...  liste_diffusion=departementok@atreal.fr
    ...  libelle=${tc_departement}
    ...  ville=MARSEILLE
    ...  accepte_notification_email=true
    ...  uid_platau_acteur=plop3
    Ajouter le tiers consulte depuis le listing  ${tc_values}
    ${tc_non_notifiable} =  Set Variable  Tiers non lié (NATC)
    &{tc_values} =  Create Dictionary
    ...  categorie_tiers_consulte=${cat_tiers}
    ...  abrege=TNLNATC
    ...  liste_diffusion=failCommuneDepartement@atreal.fr
    ...  libelle=${tc_non_notifiable}
    ...  accepte_notification_email=true
    ...  uid_platau_acteur=plop4
    Ajouter le tiers consulte depuis le listing  ${tc_values}
    ${tc_autre_type_habilitation} =  Set Variable  Tiers autre type habilitation (NATC)
    &{tc_values} =  Create Dictionary
    ...  categorie_tiers_consulte=${cat_tiers}
    ...  abrege=TATHNATC
    ...  libelle=${tc_autre_type_habilitation}
    ...  liste_diffusion=failAutreTypeHabilitation@atreal.fr
    ...  accepte_notification_email=true
    ...  uid_platau_acteur=plop5
    Ajouter le tiers consulte depuis le listing  ${tc_values}
    ${tc_tiersansid} =  Set Variable  Tiers consulté sans id (NATC)
    &{tc_values} =  Create Dictionary
    ...  categorie_tiers_consulte=${cat_tiers}
    ...  abrege=TCSINATC
    ...  liste_diffusion=sansidok@atreal.fr
    ...  libelle=${tc_tiersansid}
    ...  ville=MARSEILLE
    ...  accepte_notification_email=true
    Ajouter le tiers consulte depuis le listing  ${tc_values}

    # On crée 2 types d'habilitation de tiers consulté
    # 1 qui sera sélectionné, l'autre non
    ${type_habilitation_ok} =  Set Variable  TYPE NOTIFIE
    &{type_habilitation_tiers_consulte} =  Create Dictionary
    ...  code=456
    ...  libelle=${type_habilitation_ok}
    ...  om_validite_debut=${date_courante}
    Ajouter un type d'habilitation de tiers consulté  ${type_habilitation_tiers_consulte}
    ## On crée un deuxième type d'habilitation de tiers consulté
    ${type_habilitation_non_ok} =  Set Variable  TYPE NON NOTIFIE
    &{type_habilitation_tiers_consulte} =  Create Dictionary
    ...  code=457
    ...  libelle=${type_habilitation_non_ok}
    ...  om_validite_debut=${date_courante}
    Ajouter un type d'habilitation de tiers consulté  ${type_habilitation_tiers_consulte}

    # On ajoute 5 habilitations :
    # - 1 lié à la commune et aux départements non associés au dossier
    # - 1 lié à la commune
    # - 1 lié à la mauvaise commune mais au bon département
    # - 1 lié à la commune et au département voulu mais associé au service en charge du dossier
    # - 1 avec commune et département ok mais lié au type d'habilitation qui ne sera pas sélectionné
    @{commune_ok} =  Create List  ${code_commune_ok} - ${lib_commune_ok}
    @{dept_ok} =  Create List  ${code_dept_ok} - ${lib_dept_ok}
    @{commune_fail} =  Create List  ${code_commune_fail} - ${lib_commune_fail}
    @{dept_fail} =  Create List  ${code_dept_fail} - ${lib_dept_fail}
    &{habilitation_tiers_consulte_values} =  Create Dictionary
    ...  type_habilitation_tiers_consulte=${type_habilitation_ok}
    ...  tiers_consulte=${tc_non_notifiable}
    Ajouter une habilitation de tiers consulté  ${habilitation_tiers_consulte_values}  ${dept_fail}  ${commune_fail}
    &{habilitation_tiers_consulte_values} =  Create Dictionary
    ...  type_habilitation_tiers_consulte=${type_habilitation_ok}
    ...  tiers_consulte=${tc_commune}
    Ajouter une habilitation de tiers consulté  ${habilitation_tiers_consulte_values}  ${EMPTY}  ${commune_ok}
    &{habilitation_tiers_consulte_values} =  Create Dictionary
    ...  type_habilitation_tiers_consulte=${type_habilitation_ok}
    ...  tiers_consulte=${tc_departement}
    Ajouter une habilitation de tiers consulté  ${habilitation_tiers_consulte_values}  ${dept_ok}  ${commune_fail}
    &{habilitation_tiers_consulte_values} =  Create Dictionary
    ...  type_habilitation_tiers_consulte=${type_habilitation_ok}
    ...  tiers_consulte=${tc_tiersansid}
    Ajouter une habilitation de tiers consulté  ${habilitation_tiers_consulte_values}  ${dept_ok}  ${commune_fail}
    &{habilitation_tiers_consulte_values} =  Create Dictionary
    ...  type_habilitation_tiers_consulte=${type_habilitation_ok}
    ...  tiers_consulte=${tc_consultation_entrante}
    Ajouter une habilitation de tiers consulté  ${habilitation_tiers_consulte_values}  ${dept_ok}  ${commune_ok}
    &{habilitation_tiers_consulte_values} =  Create Dictionary
    ...  type_habilitation_tiers_consulte=${type_habilitation_non_ok}
    ...  tiers_consulte=${tc_autre_type_habilitation}
    Ajouter une habilitation de tiers consulté  ${habilitation_tiers_consulte_values}  ${dept_ok}  ${commune_ok}

    # On ajoute un événement avec notification automatique des tiers consulté
    # et on test l'affichage du champs de sélection des types d'habilitation
    @{etat_source} =  Create List  delai de notification envoye
    @{type_di} =  Create List  PCI - P - Initial
    @{type_habilitation_tiers_consulte} =  Create List  ${type_habilitation_ok}
    ${evenement_notif_auto_tc} =  Set Variable  TEST_NOTIF_AUTO_TC
    &{args_evenement} =  Create Dictionary
    ...  libelle=${evenement_notif_auto_tc}
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=test_NOTIF Test
    ...  notification_tiers=Notification automatique
    ...  type_habilitation_tiers_consulte=${type_habilitation_tiers_consulte}
    Ajouter l'événement depuis le menu  ${args_evenement}

    # Préparation de la consultation entrante du dossier
    # Change le type affichage du type de DA
    &{args_da_type} =  Create Dictionary
    ...  affichage_form=CONSULTATION ENTRANTE
    Modifier le type de dossier d'autorisation  Permis de construire  ${args_da_type}
    # Récupère le payload de création DI
    ${json_payload} =  Get File  ${EXECDIR}${/}binary_files${/}json_payload_ref.txt
    ${json_payload} =  Replace String  ${json_payload}  3XY-DK4-7X  000-AAA-00
    ${json_payload} =  Replace String  ${json_payload}  7XY-DK8-5X  AAA-000-00
    ${json_payload} =  Replace String  ${json_payload}  13055  20001
    ${json_payload} =  Replace String  ${json_payload}  SDF-ZER-74R  plop
    ${json_payload} =  Replace String  ${json_payload}  EF-DSQ-4512  plop
    ${payload_dict} =  To Json  ${json_payload}
    # Préparation de la tâche de création du dossier
    ${task_values} =  Create Dictionary
    ...  type=create_DI_for_consultation
    ...  json_payload=${json_payload}
    Ajouter la tâche par WS  ${task_values}

    # Ajout du dossier sur lequel on va déclencher la notification automatique
    # associé à la commune et à la consultation entrante voulu
    ${msg} =  Déclencher le traitement des tâches par WS
    # Récupération du numéro de dossier
    ${search_values} =  Create Dictionary
    ...  source_depot=Plat'AU
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    Depuis le contexte du dossier d'instruction par la recherche avance  ${search_values}  LIBRECOM_NOTIFDEM
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=#dossier_libelle
    ${di_notif_auto_tiers} =  Get Text  css=#dossier_libelle

    # Ajout de l'instruction declenchant la notification et saisie de la date de retour
    # de signature pour déclencher la notification des tiers
    ${inst_notif_auto} =  Ajouter une instruction au DI et la finaliser  ${di_notif_auto_tiers}  ${evenement_notif_auto_tc}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_courante}
    Click On Submit Button In Subform

    # Vérification de la présence des mails et vérification de l'affichage du suivi
    Wait until element contains  css=tr:nth-child(1) td[data-column-id="émetteur"]      admin (Administrateur) (automatique)
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="destinataire"]  Tiers consulté commune (NATC) : communeok@atreal.fr
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="instruction"]   TEST_NOTIF_AUTO_TC
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="statut"]        envoyé
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="commentaire"]   Le mail de notification a été envoyé
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="émetteur"]      admin (Administrateur) (automatique)
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="destinataire"]  Tiers consulté departement (NATC) : departementok@atreal.fr
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="instruction"]   TEST_NOTIF_AUTO_TC
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="statut"]        envoyé
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="commentaire"]   Le mail de notification a été envoyé
    Element Should Contain  css=tr:nth-child(3) td[data-column-id="émetteur"]      admin (Administrateur) (automatique)
    Element Should Contain  css=tr:nth-child(3) td[data-column-id="destinataire"]  ${tc_tiersansid} : sansidok@atreal.fr
    Element Should Contain  css=tr:nth-child(3) td[data-column-id="instruction"]   TEST_NOTIF_AUTO_TC
    Element Should Contain  css=tr:nth-child(3) td[data-column-id="statut"]        envoyé
    Element Should Contain  css=tr:nth-child(3) td[data-column-id="commentaire"]   Le mail de notification a été envoyé

    @{mail_recu} =  Create List  communeok@atreal.fr  departementok@atreal.fr  sansidok@atreal.fr
    :FOR  ${mail}  IN  @{mail_recu}
    \  Verifier que le mail a bien été envoyé au destinataire  ${mail}

    @{mail_non_recu} =  Create List  failServiceConsultant@atreal.fr  failVerifMail  failCommuneDepartement@atreal.fr  failAutreTypeHabilitation@atreal.fr
    :FOR  ${mail}  IN  @{mail_non_recu}
    \  Page Should Not Contain  ${mail}

    # Rétablissement du paramétrage

    # Rétablis le type affichage du type de DA
    &{args_da_type} =  Create Dictionary
    ...  affichage_form=ADS
    Modifier le type de dossier d'autorisation  Permis de construire  ${args_da_type}

    # Activation de l'option option_dossier_commune et de l'option option_mode_service_consulte
    &{om_param} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=option_dossier_commune
    ...  click_value=agglo
    Supprimer le paramètre (surcharge)  ${om_param}
    &{om_param} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=option_mode_service_consulte
    ...  click_value=agglo
    Supprimer le paramètre (surcharge)  ${om_param}
    # ajoute le paramètre 'acteur' à la collectivité/au service
    &{om_param} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=platau_acteur_service_consulte
    ...  click_value=LIBRECOM_NOTIFDEM
    Supprimer le paramètre (surcharge)  ${om_param}
    # définir les paramètres de type de demande
    &{om_param} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=platau_type_demande_initial_PCI
    ...  click_value=agglo
    Supprimer le paramètre (surcharge)  ${om_param}


Echec de la notification automatique par mail du demandeurs principal lié à un mauvais paramétrage
    [Documentation]  Test la notification en cas d'échec de la notification du demandeur
    ...  principale lié à un mauvais paramétrage (pas de mail, mail erroné ou n'accepte
    ...  pas les notifications).
    ...  Trois cas de notification automatique sont testés : à l'ajout de l'instruction,
    ...  à la finalisation de la lettretype et au remplissage de la date de retour de
    ...  signature.
    ...  En cas d'echec on vérifie qu'un message à destination de l'instructeur a bien été
    ...  envoyé, que le suivi de notification a bien une ligne indiquant que la notification
    ...  du demandeur principal à échoué et qu'un message d'information indiquant les
    ...  paramètre à corriger est bien affiché.

    Depuis la page d'accueil  admin  admin

    # 1er cas : notification automatique à l'ajout de l'instruction
    # Ajout d'un dossier pour lequel le demandeur principal n'accepte pas les notifications
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Fayme
    ...  particulier_prenom=Dastous
    ...  courriel=fdastous@test.fr
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction ce qui doit déclencher la création d'un message et l'apparition
    # du suivi de notification
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_auto} =  Ajouter une instruction au DI  ${di_notif_erreur}  TEST_NOTIF_AUTO
    Click On Link  ${inst_notif_auto}
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet) (automatique)
    Element Should Contain  css=td[data-column-id="destinataire"]  Fayme Dastous fdastous@test.fr
    Element Should Contain  css=td[data-column-id="instruction"]   TEST_NOTIF_AUTO
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]        Echec
    Element Should Contain  css=td[data-column-id="commentaire"]   Le pétitionnaire principal n'accepte pas les notifications.
    # Vérification qu'un message informant l'utilisateur des problèmes de paramétrages est affiché
    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO
    Element Should Contain  css=.panel_information  Attention l'envoi de notification automatique n'est pas possible.
    Click Element  css=#fieldset-message-tab_erreur_param_notif legend
    Wait until Element Is Visible  css=#fieldset-message-tab-content
    Element Should Contain  css=#fieldset-message-tab-content  Le pétitionnaire principal n'accepte pas les notifications.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  1  dossier_message
    # Récupération de l'id du premier message pour accéder plus facilement au suivant
    Page should contain  erreur expedition
    ${id_message} =  Get Text  css=td.firstcol
    Depuis le contexte du message dans le dossier d'instruction  ${di_notif_erreur}  ${id_message}
    Element Should Contain  css=#type           erreur expedition
    Element Should Contain  css=#emetteur       mpaulet (Mandel Paulet)
    Element Should Contain  css=#destinataire   instructeur
    Element Should Contain  css=#lu             Non
    Element Should Contain  css=#contenu        Échec lors de la notification de l'instruction TEST_NOTIF_AUTO.\nLe pétitionnaire principal n'accepte pas les notifications.\nVeuillez corriger ces informations avant de renvoyer la notification.



    # 2ème cas : notification automatique à la finalisation du document d'instruction
    # Ajout d'un dossier pour lequel le demandeur principal n'a pas d'adresse mail
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Leala
    ...  particulier_prenom=Rocheleau
    ...  notification=t
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction ce qui doit déclencher la création d'un message et l'apparition
    # du suivi de notification
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_erreur}  TEST_NOTIF_AUTO_LETTRETYPE
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet) (automatique)
    Element Should Contain  css=td[data-column-id="destinataire"]  Leala Rocheleau
    Element Should Contain  css=td[data-column-id="instruction"]   TEST_NOTIF_AUTO_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]        Echec
    Element Should Contain  css=td[data-column-id="commentaire"]   Le courriel du pétitionnaire principal n'est pas renseigné.
    # Vérification qu'un message informant l'utilisateur des problèmes de paramétrages est affiché
    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO_LETTRETYPE
    Element Should Contain  css=.panel_information  Attention l'envoi de notification automatique n'est pas possible.
    Click Element  css=#fieldset-message-tab_erreur_param_notif legend
    Wait until Element Is Visible  css=#fieldset-message-tab-content
    Element Should Contain  css=#fieldset-message-tab-content  Le courriel du pétitionnaire principal n'est pas renseigné.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  1  dossier_message
    # Récupération de l'id du premier message pour accéder plus facilement au suivant
    ${id_message2} =  Get Text  css=td.firstcol
    Depuis le contexte du message dans le dossier d'instruction  ${di_notif_erreur}  ${id_message2}
    Element Should Contain  css=#type           erreur expedition
    Element Should Contain  css=#emetteur       mpaulet (Mandel Paulet)
    Element Should Contain  css=#destinataire   instructeur
    Element Should Contain  css=#lu             Non
    Element Should Contain  css=#contenu        Échec lors de la notification de l'instruction TEST_NOTIF_AUTO_LETTRETYPE.\nLe courriel du pétitionnaire principal n'est pas renseigné.



    # 3ème cas : notification automatique après retour signature du document d'instruction
    # Ajout d'un dossier pour lequel le demandeur principal a une adresse mail incorrect
    # et pour lequel il y a 2 demandeurs
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Boileau
    ...  particulier_prenom=Daniel
    ...  courriel=bdaniel.oups
    ...  notification=t
    ...  om_collectivite=LIBRECOM_NOTIFDEM

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Grondin
    ...  particulier_prenom=Orson
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=ogrondin@notif.fr
    ...  notification=t
    ...  om_collectivite=LIBRECOM_NOTIFDEM

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout d'une instruction ce qui doit déclencher la création d'un message et l'apparition
    # du suivi de notification
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_notif_erreur}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform
    # Vérification du suivi de la notification
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="émetteur"]      admin (Administrateur)
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="destinataire"]  Boileau Daniel bdaniel.oups
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="instruction"]   TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="statut"]        Echec
    Element Should Contain  css=tr:nth-child(2) td[data-column-id="commentaire"]   Le courriel du pétitionnaire principal n'est pas correct : bdaniel.oups.
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="émetteur"]      admin (Administrateur)
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="destinataire"]  Grondin Orson ogrondin@notif.fr
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="instruction"]   TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="statut"]        envoyé
    Element Should Contain  css=tr:nth-child(1) td[data-column-id="commentaire"]   Le mail de notification a été envoyé
    # Vérification qu'un message informant l'utilisateur des problèmes de paramétrages est affiché
    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Element Should Contain  css=.panel_information  Attention l'envoi de notification automatique n'est pas possible.
    Click Element  css=#fieldset-message-tab_erreur_param_notif legend
    Wait until Element Is Visible  css=#fieldset-message-tab-content
    Element Should Contain  css=#fieldset-message-tab-content  Le courriel du pétitionnaire principal n'est pas correct : bdaniel.oups.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  1  dossier_message
    # Récupération de l'id du premier message pour accéder plus facilement au suivant
    ${id_message3} =  Get Text  css=td.firstcol
    Depuis le contexte du message dans le dossier d'instruction  ${di_notif_erreur}  ${id_message3}
    Element Should Contain  css=#type           erreur expedition
    Element Should Contain  css=#emetteur       admin (Administrateur)
    Element Should Contain  css=#destinataire   instructeur
    Element Should Contain  css=#lu             Non
    Element Should Contain  css=#contenu        Le courriel du pétitionnaire principal n'est pas correct : bdaniel.oups.


Desactivation de la notification par mail
    [Documentation]  Desactive la notification par mail des demandeurs

    Depuis la page d'accueil  admin  admin

    &{param_args} =  Create Dictionary
    ...  selection_col=libellé
    ...  search_value=option_notification
    ...  click_value=LIBRECOM_NOTIFDEM
    Supprimer le paramètre (surcharge)  ${param_args}
    
    Arrêter maildump


Notification automatique via le portail citoyen d'une instruction sans lettretype
    [Documentation]  Vérifie le bon fonctionnement de la notification automatique
    ...  via le portail citoyen des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=L'Heureux
    ...  particulier_prenom=Madeleine
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=mlheureux@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Charette
    ...  particulier_prenom=Suzette
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=scharette@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique sans lettretype
    # La notification doit se faire à l'ajout de l'instruction
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_auto1} =  Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_AUTO
    Click On Link  ${inst_notif_auto1}
    Element Should Contain  css=div#suivi_notification_jsontotab  mlheureux@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  en cours d'envoi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  scharette@notif.fr
    Portlet Action Should Be In SubForm  instruction  notification_manuelle_portal


Notification automatique via le portail citoyen d'une instruction avec lettretype sans signature requise
    [Documentation]  Vérifie le bon fonctionnement de la notification automatique
    ...  via le portail citoyen des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Desjardins
    ...  particulier_prenom=Sargent
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=sdesjardins@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Boisclair
    ...  particulier_prenom=Rabican
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=rboisclair@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique sans la finaliser pour vérifier
    # que l'action d'envoi manuelle de la notification n'est pas visible
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_AUTO_LETTRETYPE
    Portlet Action Should Not Be In SubForm  instruction  notification_manuelle_portal
    # Finalisation de l'instruction ce qui doit déclencher l'envoi de la notification automatique
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_AUTO_LETTRETYPE
    Click On SubForm Portlet Action  instruction  finaliser
    Element Should Contain  css=div#suivi_notification_jsontotab  sdesjardins@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  en cours d'envoi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  rboisclair@notif.fr
    Portlet Action Should Be In SubForm  instruction  notification_manuelle_portal


Notification automatique via le portail citoyen d'une instruction avec lettretype et avec retour signature
    [Documentation]  Vérifie le bon fonctionnement de la notification automatique
    ...  via le portail citoyen des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=Denomination
    ...  personne_morale_nom=Tabor
    ...  personne_morale_prenom=Phaneuf
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=tphaneuf@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_raison_sociale=raison sociale
    ...  personne_morale_nom=Labrosse
    ...  personne_morale_prenom=Patrick
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=plabosse@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique avec lettretype avec signature
    # La notification doit se faire à l'ajout de la date de retour
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Portlet Action Should Not Be In SubForm  instruction  notification_manuelle_portal
    Page Should Not Contain Element  css=fieldset#fieldset-sousform-instruction-suivi-notification
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform
    # Vérification des infos
    Element Should Contain  css=div#suivi_notification_jsontotab  tphaneuf@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  en cours d'envoi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  plabosse@notif.fr
    Portlet Action Should Be In SubForm  instruction  notification_manuelle_portal


Notification manuelle via le portail citoyen d'une instruction sans lettretype
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  via le portail citoyen des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Chandonnet
    ...  particulier_prenom=Honoré
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=hchandonnet@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Dufresne
    ...  particulier_prenom=Villette
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=vdufresne@notnotif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique sans lettretype
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_man} =  Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_MAN
    CLick On Link  ${inst_notif_man}
    # L'action ne doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  notification_manuelle_portal

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  notification_manuelle_portal  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Element Should Contain  css=div#suivi_notification_jsontotab  hchandonnet@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  en cours d'envoi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  vdufresne@notnotif.fr


Notification manuelle via le portail citoyen d'une instruction avec lettretype sans signature requise
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  via le portail citoyen des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=denom1
    ...  personne_morale_nom=Lapierre
    ...  personne_morale_prenom=Ormazd
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=olapierre@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=denom2
    ...  personne_morale_nom=Poisson
    ...  personne_morale_prenom=Warrane
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=pwarrane@notnotif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification manuelle avec lettretype sans signature
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE
    # L'action doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE
    Portlet Action Should Be In SubForm  instruction  notification_manuelle_portal

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  notification_manuelle_portal  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Element Should Contain  css=div#suivi_notification_jsontotab  olapierre@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  en cours d'envoi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  pwarrane@notnotif.fr


Notification manuelle via le portail citoyen d'une instruction avec lettretype et signature requise
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  via le portail citoyen des demandeurs

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    # Seul le pétitionnaire principal a un courriel et accepte les notification
    # c'est donc le seul pétitionnaire qui devra être notifié
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Doucet
    ...  particulier_prenom=Merle
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=mdoucet@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Riel
    ...  particulier_prenom=Chappell
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=criel@notnotif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # Ajout de l'instruction de notification automatique avec lettretype avec signature
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE
    # L'action ne doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE
    Portlet Action Should Not Be In SubForm  instruction  notification_manuelle_portal
    # Ajout d'une date de signature
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform
    Portlet Action Should Be In SubForm  instruction  notification_manuelle_portal

    # Utilisation de l'action
    Click On SubForm Portlet Action  instruction  notification_manuelle_portal  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Element Should Contain  css=div#suivi_notification_jsontotab  mdoucet@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  en cours d'envoi
    Element Should Not Contain  css=div#suivi_notification_jsontotab  criel@notnotif.fr


Notification manuelle via le portail citoyen d'une instruction sans lettretype avec annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin
    # Ajout d'un dossier et d'une instruction de notification manuelle sans lettretype
    # avec une annexe
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Lang
    ...  particulier_prenom=Roxanne
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=rlang@notif.fr
    ...  notification=t

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # finalisation et ajout d'une date de retour signature sur une instruction
    # pour pouvoir la choisir comme annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de l'instruction de notification
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_man_annexe} =  Ajouter une instruction au DI  ${di_notif_auto1}  TEST_NOTIF_MAN_ANNEXE
    Click On Link  ${inst_notif_man_annexe}
    # L'action ne doit être dans le portlet
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'affichage du formulaire de notif manuelle
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    # Sélection d'une annexe et validation
    Page Should Not Contain Element  css=div#instruction_notification_manuelle input[type="checkbox"]
    @{liste_documents}  Create List  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Text Should Be  css=td[data-column-id="dateD'envoi"]   ${EMPTY}
    Element Should Contain  css=td[data-column-id="destinataire"]  rlang@notif.fr
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN_ANNEXE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe
    Element Should Contain  css=td[data-column-id="statut"]       en cours d'envoi
    Element Should Contain  css=td[data-column-id="commentaire"]  Notification en cours de traitement
    Page Should Not Contain Element  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}


Notification manuelle via le portail citoyen d'une instruction avec lettretype avec annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin
    # Ajout d'un dossier et d'une instruction de notification manuelle sans lettretype
    # avec une annexe
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Chartré
    ...  particulier_prenom=Arnaud
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=achartre@notif.fr
    ...  notification=t

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # finalisation et ajout d'une date de retour signature sur une instruction du di
    # pour pouvoir la choisir comme annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de l'instruction et finalisation
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    # L'action doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'affichage du formulaire de notif manuelle avec annexe
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    Wait Until Page Contains Element  css=div#annexes_documents_chosen
    # Sélection d'une annexe et validation
    Page Should Not Contain Element  css=div#instruction_notification_manuelle input[type="checkbox"]
    @{liste_documents}  Create List  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet)
    Element Text Should Be  css=td[data-column-id="dateD'envoi"]   ${EMPTY}
    Element Should Contain  css=td[data-column-id="destinataire"]  achartre@notif.fr
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe
    Element Should Contain  css=td[data-column-id="statut"]       en cours d'envoi
    Element Should Contain  css=td[data-column-id="commentaire"]  Notification en cours de traitement
    Page Should Not Contain Element  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}


Notification manuelle via le portail citoyen d'une instruction avec lettretype, signature requise et annexe
    [Documentation]  Vérifie le bon fonctionnement de la notification manuelle
    ...  par mail des demandeurs

    Depuis la page d'accueil  admin  admin
    # Ajout d'un dossier et d'une instruction de notification manuelle sans lettretype
    # avec une annexe
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Rochefort
    ...  particulier_prenom=Algernon
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=arochefort@notif.fr
    ...  notification=t

    &{args_petitionnaire1} =  Create Dictionary
    ...  particulier_nom=Landry
    ...  particulier_prenom=Logistilla
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=llandry@notif.fr
    ...  notification=t

    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire1}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_auto1} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    # finalisation et ajout d'une date de retour signature sur une instruction du di
    # pour pouvoir la choisir comme annexe
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de l'instruction et finalisation
    Ajouter une instruction au DI et la finaliser  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    # L'action ne doit être dans le portlet
    Depuis l'instruction du dossier d'instruction  ${di_notif_auto1}  TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    Portlet Action Should Not Be In SubForm  instruction  overlay_notification_manuelle
    # Ajout d'une date de signature
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform
    Portlet Action Should Be In SubForm  instruction  overlay_notification_manuelle

    # Vérification de l'affichage du formulaire de notif manuelle :
    # les 2 pétitionnaires et le champs de sélection de l'annexe doivent être visible
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale
    # Sélection d'un demandeur et validation
    Wait Until Page Contains Element  css=div#annexes_documents_chosen
    Page Should Not Contain Element  css=div#instruction_notification_manuelle input[type="checkbox"]
    @{liste_documents}  Create List  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    Wait Until Page Contains  La notification a été générée.

    # Vérifie que la page s'est bien mis à jour lors de la validation
    Wait until element contains  css=#fieldset-sousform-instruction-suivi-notification  Suivi notification
    # Test de l'affichage des informations dans le tableau de suivi
    
    Element Should Contain  css=td[data-column-id="émetteur"]      admin (Administrateur)
    Element Text Should Be  css=td[data-column-id="dateD'envoi"]   ${EMPTY}
    Element Should Contain  css=td[data-column-id="destinataire"]  arochefort@notif.fr
    Element Should Contain  css=td[data-column-id="instruction"]  TEST_NOTIF_MAN_SIGN_LETTRETYPE_ANNEXE
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe
    Element Should Contain  css=td[data-column-id="statut"]       en cours d'envoi
    Element Should Contain  css=td[data-column-id="commentaire"]  Notification en cours de traitement
    Page Should Not Contain Element  css=td[data-column-id="dateDePremierAccès"]  ${EMPTY}


Suppression d'une instruction liée à une notification via le portail citoyen
    [Documentation]  Vérifie le comportement suite à la suppression d'instruction ayant
    ...  été notifiée

    Depuis la page d'accueil  admin  admin

    # Ajout d'un dossier et d'une instruction de notification auto
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Dupuis
    ...  particulier_prenom=Varden
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=vdupuis@notif.fr
    ...  notification=t
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_del} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}
    ${di_notif_del_se} =  Sans espace  ${di_notif_del}

    # Ajout de l'instruction de notification automatique sans lettretype
    # La notification doit se faire à l'ajout de l'instruction
    ${inst_notif_auto2} =  Ajouter une instruction au DI  ${di_notif_del}  TEST_NOTIF_AUTO
    Click On Link  ${inst_notif_auto2}
    Element Should Contain  css=div#suivi_notification_jsontotab  vdupuis@notif.fr
    Element Should Contain  css=div#suivi_notification_jsontotab  en cours d'envoi
    # Vérification de la task
    &{task_values} =  Create Dictionary
    ...  type=notification_instruction
    ...  dossier=${di_notif_del_se}
    ...  state=new
    ...  link_dossier=${di_notif_del_se}
    ...  stream=output
    Vérifier que la tâche a bien été ajoutée ou modifiée  ${task_values}  portal

    # Suppression de l'instruction
    Supprimer l'instruction  ${di_notif_del}  TEST_NOTIF_AUTO
    # Vérification de la task
    &{task_values} =  Create Dictionary
    ...  type=notification_instruction
    ...  dossier=${di_notif_del_se}
    ...  state=canceled
    ...  link_dossier=${di_notif_del_se}
    ...  stream=output
    Vérifier que la tâche a bien été ajoutée ou modifiée  ${task_values}  portal


Suppression d'un dossier d'instruction ayant son récépissé notifié
    [Documentation]  Vérifie le comportement suite à la suppression d'un dossier
    ...  d'instruction ayant son récépissé notifié

    Depuis la page d'accueil  admin  admin

    # Active l'option de suppression des dossiers
    &{om_param} =  Create Dictionary
    ...  libelle=option_suppression_dossier_instruction
    ...  valeur=true
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}

    # Modifie l'événement de récépissé
    &{args_evenement} =  Create Dictionary
    ...  libelle=Notification du delai legal maison individuelle
    ...  notification=Notification automatique
    Modifier l'événement  ${args_evenement}

    # Ajout d'un dossier et d'une instruction de notification auto
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Patry
    ...  particulier_prenom=Robert
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=rpatry@notif.fr
    ...  notification=t
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_del} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}
    ${di_del_se} =  Sans espace  ${di_del}

    # Vérification de la task
    &{task_values} =  Create Dictionary
    ...  type=notification_recepisse
    ...  dossier=${di_del_se}
    ...  state=new
    ...  link_dossier=${di_del_se}
    ...  stream=output
    Vérifier que la tâche a bien été ajoutée ou modifiée  ${task_values}  portal

    # Supprime le dossier d'instruction
    Supprimer le dossier d'instruction  ${di_del}

    # Vérification de la task
    &{task_values} =  Create Dictionary
    ...  type=notification_recepisse
    ...  dossier=${di_del_se}
    ...  state=canceled
    ...  link_dossier=${di_del_se}
    ...  stream=output
    Vérifier que la tâche a bien été ajoutée ou modifiée  ${task_values}  portal

    # Suppression des paramètres
    &{args_evenement} =  Create Dictionary
    ...  libelle=Notification du delai legal maison individuelle
    ...  notification=Pas de notification
    Modifier l'événement  ${args_evenement}
    &{om_param} =  Create Dictionary
    ...  libelle=option_suppression_dossier_instruction
    ...  valeur=false
    ...  om_collectivite=agglo
    Ajouter ou modifier le paramètre depuis le menu  ${om_param}


Gestion des erreurs de paramétrage pour les dossiers notifiés via le portail citoyen
    [Documentation]  Ce test vérifie que si le dossier a été déposé via le portail citoyen
    ...  alors, même si le paramétrage du pétitionnaire principal n'est pas correct, le
    ...  message d'information pour la correction ne sera pas affiché sur l'instruction.
    ...  Vérifie également que les problèmes de notification n'empêche pas le déclenchement
    ...  de la notification et qu'il n'y a pas d'alert envoyé en cas de problème de paramétrage.
    ...  Pour les dossiers non déposés via le portail citoyen vérifie que les erreurs de
    ...  paramétrage du demandeur principal ne déclenche pas de notification et de message
    ...  indiquant une erreur de paramétrage.

    # 1er cas : notification automatique à l'ajout de l'instruction
    # Ajout d'un dossier pour lequel le demandeur principal n'accepte pas les notifications
    # et n'a pas d'adresse mail
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Quessy
    ...  particulier_prenom=Apolline
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ...  source_depot=portal
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction le message d'information concernant les erreurs de paramétrage
    # ne dois pas être affiché et l'instructeur ne doit pas recevoir de message
    # La notification doit être créé
    Depuis la page d'accueil  mpaulet  mpaulet
    ${inst_notif_auto3} =  Ajouter une instruction au DI  ${di_notif_erreur}  TEST_NOTIF_AUTO
    Click On Link  ${inst_notif_auto3}
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet) (automatique)
    Element Should Contain  css=td[data-column-id="destinataire"]  Quessy Apolline
    Element Should Contain  css=td[data-column-id="instruction"]   TEST_NOTIF_AUTO
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]        en cours d'envoi
    Element Should Contain  css=td[data-column-id="commentaire"]   Notification en cours de traitement

    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO
    Page Should Not Contain  Attention l'envoi de notification automatique n'est pas possible.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  0  dossier_message

    # Cas 2 : notification automatique à l'ajout de l'instruction pour un dossier qui n'a pas été
    # déposé via portal.
    # Ajout d'un dossier pour lequel le demandeur principal n'accepte pas les notifications
    # et n'a pas d'adresse mail.
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Déziel
    ...  particulier_prenom=Agathe
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ...  source_depot=app
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction, il ne doit pas y avoir de notification ni d'alerte.
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI  ${di_notif_erreur}  TEST_NOTIF_AUTO

    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO
    Page Should Not Contain Element  css=div#suivi_notification_jsontotab
    Page Should Not Contain  Attention l'envoi de notification automatique n'est pas possible.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  0  dossier_message

    # Cas 3 : notification automatique à la finalisation du document d'instruction
    # Ajout d'un dossier pour lequel le demandeur principal n'accepte pas les notifications
    # et n'a pas d'adresse mail
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Aymon
    ...  particulier_prenom=Cailot
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ...  source_depot=portal
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction le message d'information concernant les erreurs de paramétrage
    # ne dois pas être affiché et l'instructeur ne doit pas recevoir de message
    # La notification doit être créé
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_erreur}  TEST_NOTIF_AUTO_LETTRETYPE
    Element Should Contain  css=td[data-column-id="émetteur"]      mpaulet (Mandel Paulet) (automatique)
    Element Should Contain  css=td[data-column-id="destinataire"]  Aymon Cailot
    Element Should Contain  css=td[data-column-id="instruction"]   TEST_NOTIF_AUTO_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]        en cours d'envoi
    Element Should Contain  css=td[data-column-id="commentaire"]   Notification en cours de traitement

    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO_LETTRETYPE
    Page Should Not Contain  Attention l'envoi de notification automatique n'est pas possible.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  0  dossier_message

    # Cas 4 : notification automatique à la finalisation du document d'instruction pour un dossier
    # qui n'a pas été déposé via portal.
    # Ajout d'un dossier pour lequel le demandeur principal n'accepte pas les notifications
    # et n'a pas d'adresse mail. Il ne doit pas y avoir de notification en erreur ni de message
    # d'alerte
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Dupuis
    ...  particulier_prenom=Dielle
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ...  source_depot=app
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction, la notification ne doit pas être ajouté et il ne doit pas y avoir d'alerte
    Depuis la page d'accueil  mpaulet  mpaulet
    Ajouter une instruction au DI et la finaliser  ${di_notif_erreur}  TEST_NOTIF_AUTO_LETTRETYPE

    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO_LETTRETYPE
    Page Should Not Contain Element  css=div#suivi_notification_jsontotab
    Page Should Not Contain  Attention l'envoi de notification automatique n'est pas possible.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  0  dossier_message


    # Cas 5 : notification automatique après retour signature.
    # Ajout d'un dossier pour lequel le demandeur principal n'accepte pas les notifications
    # et n'a pas d'adresse mail
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Lang
    ...  particulier_prenom=Grégoire
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ...  source_depot=portal
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction le message d'information concernant les erreurs de paramétrage
    # ne dois pas être affiché et l'instructeur ne doit pas recevoir de message
    # La notification doit être créé
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_notif_erreur}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform
    # Vérification du suivi de la notification
    Element Should Contain  css=td[data-column-id="émetteur"]      admin (Administrateur)
    Element Should Contain  css=td[data-column-id="destinataire"]  Lang Grégoire
    Element Should Contain  css=td[data-column-id="instruction"]   TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Element Text Should Be  css=td[data-column-id="annexes"]  ${EMPTY}
    Element Should Contain  css=td[data-column-id="statut"]        en cours d'envoi
    Element Should Contain  css=td[data-column-id="commentaire"]   Notification en cours de traitement

    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Page Should Not Contain  Attention l'envoi de notification automatique n'est pas possible.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  0  dossier_message

    # Cas 6 : notification automatique après retour signature pour un dossier
    # qui n'a pas été déposé via portal.
    # Ajout d'un dossier pour lequel le demandeur principal n'accepte pas les notifications
    # et n'a pas d'adresse mail
    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Duval
    ...  particulier_prenom=Arnaud
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ...  source_depot=app
    ${di_notif_erreur} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # Ajout d'une instruction, la notification ne doit pas être ajouté et il ne doit pas y avoir d'alerte
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_notif_erreur}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    # Remplissage de la date de retour de signature
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    Depuis l'instruction du dossier d'instruction  ${di_notif_erreur}  TEST_NOTIF_AUTO_SIGN_LETTRETYPE
    Page Should Not Contain Element  css=div#suivi_notification_jsontotab
    Page Should Not Contain  Attention l'envoi de notification automatique n'est pas possible.

    Depuis l'onglet des messages du dossier d'instruction  ${di_notif_erreur}
    Total Results In Subform Should Be Equal  0  dossier_message


Notification avec annexes multiples via le portail citoyen
    [Documentation]  Vérifie à l'ouverture du formulaire de notification que le
    ...  message d'information indique bien le nombre maximum d'annexes acceptées.
    ...  Vérifie que si l'utilisateur sélectionne plus de 5 annexes un message
    ...  d'erreur s'affiche à la validation du formulaire et le formulaire est
    ...  re-affiché.
    ...  Vérifie également que dans le tableau de suivi des notifications la date de
    ...  premier accès n'est pas présente.

    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=DeGrasse
    ...  particulier_prenom=Charlot
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  courriel=dcharlot@notif.fr
    ...  notification=t
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=LIBRECOM_NOTIFDEM
    ...  depot_electronique=true
    ${di_notif_annexe_mult} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}

    # finalisation et ajout d'une date de retour signature sur une instruction du di
    # pour pouvoir la choisir comme annexe
    Depuis la page d'accueil  admin  admin
    Depuis l'instruction du dossier d'instruction  ${di_notif_annexe_mult}  Notification du delai legal maison individuelle
    ${date_retour_sign} =  Convert Date  ${DATE_FORMAT_YYYY-MM-DD}  result_format=%d/%m/%Y
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_signature  ${date_retour_sign}
    Click On Submit Button In Subform

    # Ajout de 4 pièces qui pourront être sélectionnées comme annexe
    @{liste_pieces}  Create List  autres pièces composant le dossier (A0)  arrêté retour préfecture  certificat conformité totale lotissement  avis obligatoires
    @{title_piece_annexes}  Create List
    : FOR  ${piece}  IN  @{liste_pieces}
    \  &{document_numerise_values} =  Create Dictionary
    \  ...  uid_upload=testImportManuel.pdf
    \  ...  date_creation=10/09/2016
    \  ...  document_numerise_type=${piece}
    \  Ajouter une pièce depuis le dossier d'instruction  ${di_notif_annexe_mult}  ${document_numerise_values}
    # Récupère le nom du fichier et l'associe à celui de la pièce pour obtenir le titre de l'annexe
    \  Click On Back Button In SubForm
    \  ${nom_fichier} =  Get Text  xpath=//a[normalize-space(text()) = "${piece}"]//ancestor::tr/td[contains(@class, "firstcol")]/a/span[contains(@title, "Télécharger")]
    \  ${title_annexe} =  Catenate  ${nom_fichier}  -  ${piece}
    \  Append To List  ${title_piece_annexes}  ${title_annexe}
    # Supprime le dernier titre car la dernière pièce ne sera pas transmise lors de la notification
    Remove From List  ${title_piece_annexes}  3

    # Ajout d'une consultation et rendu d'avis pour pouvoir la choisir comme annexe
    Ajouter une consultation depuis un dossier  ${di_notif_annexe_mult}  00.02 - ServiceNonNotifiable
    Depuis le contexte de la consultation  ${di_notif_annexe_mult}  00.02 - ServiceNonNotifiable
    &{piece_values} =  Create Dictionary
    ...  fichier_upload=testImportManuel2.pdf
    ...  date_demande=03/02/2016
    ...  avis_consultation=Tacite
    ${nom_piece} =  Ajouter une pièce à la consultation  ${piece_values}

    # Connexion en tant qu'instructeur du dossier
    # Ajout d'une instruction notifiable à laquelle on peut ajouter des annexes
    Ajouter une instruction au DI et la finaliser  ${di_notif_annexe_mult}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE

    # Accès au formulaire de notification manuelle et vérification du message d'info
    Depuis la page d'accueil  mpaulet  mpaulet
    Depuis l'instruction du dossier d'instruction  ${di_notif_annexe_mult}  TEST_NOTIF_MAN_LETTRETYPE_ANNEXE
    Click On SubForm Portlet Action  instruction  overlay_notification_manuelle  modale

    # Sélection de toutes les annexes possibles
    Select From Multiple Chosen List  annexes_pieces  ${liste_pieces}
    @{liste_documents}  Create List  Avis - ServiceNonNotifiable  Notification du delai legal maison individuelle
    Select From Multiple Chosen List  annexes_documents  ${liste_documents}

    # Validation du formulaire et vérification du message d'erreur
    Click Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]
    Wait until keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL} 
    ...  Error Message Should Contain  Plus de 5 annexes ont été sélectionnées vous devez en supprimer 1 pour que les pétitionnaires soient notifiés.

    # Déselection de certaines pièces pour n'en garder que 5
    @{liste_pieces_unselect}  Create List  avis obligatoires
    Unselect From Multiple Chosen List  annexes_pieces  ${liste_pieces_unselect}
    Click Element Until New Element  css=div#sousform-instruction_notification_manuelle input[type="submit"]  css=.message.ui-state-valid
    Wait Until Element Contains 
    ...  css=.message.ui-state-valid 
    ...  La notification a été générée.\nLes pièces et documents suivants seront envoyés :\nTEST_NOTIF_MAN_LETTRETYPE_ANNEXE\ncertificat conformité totale lotissement\nautres pièces composant le dossier (A0)\narrêté retour préfecture\nAvis - ServiceNonNotifiable\nNotification du delai legal maison individuelle

    # Affichage de la liste des annexes dans le tableau de suivi
    Click Link  css=.ui-dialog-titlebar-close
    Wait Until Page Contains Element  css=td[data-column-id="annexes"]
    Element Text Should Be  css=td[data-column-id="annexes"]  Annexe\nAnnexe\nAnnexe\nAnnexe\nAnnexe

    # Affichage du nom de l'élement dans le tooltip. Pour ça on vérifie que l'élément contiens bien
    # un attribut title ayant le nom de la pièce
    # Récupération des attributs des annexes et stockage dans une liste
    # On vérifie également l'affichage de la page de téléchargement du document
    @{contenu_annexes}  Create List    TEST IMPORT MANUEL 1  TEST IMPORT MANUEL 1  TEST IMPORT MANUEL 1  TEST IMPORT MANUEL 2  RECEPISSE DE DEPOT
    @{liste_titre_annexes}  Create List
    : FOR  ${index}  IN RANGE  1  6
    \  ${tooltip} =  Get Element Attribute  css=td[data-column-id="annexes"] li:nth-child(${index}) a  title
    \  Append To List  ${liste_titre_annexes}  ${tooltip}
    \  Click Link  css=td[data-column-id="annexes"] li:nth-child(${index}) a
    # Récupération du contenu du document et vérification du contenu dans la page
    # de téléchargement
    \  ${index_contenu} =  Evaluate  ${index} - 1
    \  ${contenu} =  Get From List  ${contenu_annexes}  ${index_contenu}
    \  Select Window  NEW
    \  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${contenu}
    # Ferme la fenêtre de récupération du document et retourne sur l'application
    \  Close Window
    \  Select Window
    # Vérifie que les titres des documents existe bien dans la liste
    List Should Contain Sub List  ${liste_titre_annexes}  ${liste_documents}
    # Vérifie que les titres des pièce existe bien dans la liste
    List Should Contain Sub List  ${liste_titre_annexes}  ${title_piece_annexes}
