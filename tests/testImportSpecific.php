<?php
/**
 * Ce script contient la définition de la classe 'ImportSpecificTest'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "testImportSpecific_common.php";
final class ImportSpecificTest extends ImportSpecificCommon {
    public function setUp(): void {
        $this->common_setUp();
    }
    public function tearDown(): void {
        $this->common_tearDown();
    }
    public function onNotSuccessfulTest(Throwable $e): void {
        $this->common_onNotSuccessfulTest($e);
    }
}
