<?php
/**
 * Ce fichier permet d'afficher le portail d'accès citoyen aux utilisateurs
 * anonymes.
 *
 * @package openfoncier
 * @version SVN : $Id$
 */

require_once "web.class.php";
$f = new openads_web("anonym");
$inst = $f->get_inst__om_dbform(array(
    "obj" => "dossier_autorisation",
    "idx" => 0,
));
$inst->view_consulter_anonym($f->get_content_only_param());
