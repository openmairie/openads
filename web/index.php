<?php
/**
 * Ce fichier permet de faire une redirection vers la page d'accès du portail
 * citoyen.
 *
 * @package openfoncier
 * @version SVN : $Id$
 */

//
header("Location: citizen.php");

?>
